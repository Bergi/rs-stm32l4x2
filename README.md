
# `stm32l4x2`

> Memory map for STM32L4x2 microcontrollers

## [Documentation](https://docs.rs/stm32l4x2)

## License

Licensed under either of:

- Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or
  http://www.apache.org/licenses/LICENSE-2.0)
- MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall
be dual licensed as above, without any additional terms or conditions.

## Thanks

This crate is made possible by Jorge Aparicio and all of the others that have
contributed to svd2rust.
Special thanks to Brandon Edens, whose https://github.com/brandonedens/stm32l4x6
served as the inspiration of this crate.