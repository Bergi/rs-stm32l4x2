#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::FPSCR {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bit 0 - Invalid operation cumulative exception bit"]
    #[inline]
    pub fn ioc(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 1 - Division by zero cumulative exception bit."]
    #[inline]
    pub fn dzc(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 2 - Overflow cumulative exception bit"]
    #[inline]
    pub fn ofc(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 3 - Underflow cumulative exception bit"]
    #[inline]
    pub fn ufc(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 3;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 4 - Inexact cumulative exception bit"]
    #[inline]
    pub fn ixc(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 4;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 7 - Input denormal cumulative exception bit."]
    #[inline]
    pub fn idc(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 7;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bits 22:23 - Rounding Mode control field"]
    #[inline]
    pub fn rmode(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 22;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bit 24 - Flush-to-zero mode control bit:"]
    #[inline]
    pub fn fz(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 24;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 25 - Default NaN mode control bit"]
    #[inline]
    pub fn dn(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 25;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 26 - Alternative half-precision control bit"]
    #[inline]
    pub fn ahp(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 26;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 28 - Overflow condition code flag"]
    #[inline]
    pub fn v(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 28;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 29 - Carry condition code flag"]
    #[inline]
    pub fn c(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 29;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 30 - Zero condition code flag"]
    #[inline]
    pub fn z(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 30;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 31 - Negative condition code flag"]
    #[inline]
    pub fn n(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 31;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bit 0 - Invalid operation cumulative exception bit"]
    #[inline]
    pub fn clear_ioc(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 0);
        self
    }
    #[doc = "Bit 0 - Invalid operation cumulative exception bit"]
    #[inline]
    pub fn set_ioc(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 0;
        self
    }
    #[doc = "Bit 0 - Invalid operation cumulative exception bit"]
    #[inline]
    pub fn ioc(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 1 - Division by zero cumulative exception bit."]
    #[inline]
    pub fn clear_dzc(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 1);
        self
    }
    #[doc = "Bit 1 - Division by zero cumulative exception bit."]
    #[inline]
    pub fn set_dzc(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 1;
        self
    }
    #[doc = "Bit 1 - Division by zero cumulative exception bit."]
    #[inline]
    pub fn dzc(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 2 - Overflow cumulative exception bit"]
    #[inline]
    pub fn clear_ofc(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 2);
        self
    }
    #[doc = "Bit 2 - Overflow cumulative exception bit"]
    #[inline]
    pub fn set_ofc(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 2;
        self
    }
    #[doc = "Bit 2 - Overflow cumulative exception bit"]
    #[inline]
    pub fn ofc(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 3 - Underflow cumulative exception bit"]
    #[inline]
    pub fn clear_ufc(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 3);
        self
    }
    #[doc = "Bit 3 - Underflow cumulative exception bit"]
    #[inline]
    pub fn set_ufc(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 3;
        self
    }
    #[doc = "Bit 3 - Underflow cumulative exception bit"]
    #[inline]
    pub fn ufc(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 3;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 4 - Inexact cumulative exception bit"]
    #[inline]
    pub fn clear_ixc(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 4);
        self
    }
    #[doc = "Bit 4 - Inexact cumulative exception bit"]
    #[inline]
    pub fn set_ixc(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 4;
        self
    }
    #[doc = "Bit 4 - Inexact cumulative exception bit"]
    #[inline]
    pub fn ixc(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 4;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 7 - Input denormal cumulative exception bit."]
    #[inline]
    pub fn clear_idc(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 7);
        self
    }
    #[doc = "Bit 7 - Input denormal cumulative exception bit."]
    #[inline]
    pub fn set_idc(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 7;
        self
    }
    #[doc = "Bit 7 - Input denormal cumulative exception bit."]
    #[inline]
    pub fn idc(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 7;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 22:23 - Rounding Mode control field"]
    #[inline]
    pub unsafe fn rmode(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 22;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 24 - Flush-to-zero mode control bit:"]
    #[inline]
    pub fn clear_fz(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 24);
        self
    }
    #[doc = "Bit 24 - Flush-to-zero mode control bit:"]
    #[inline]
    pub fn set_fz(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 24;
        self
    }
    #[doc = "Bit 24 - Flush-to-zero mode control bit:"]
    #[inline]
    pub fn fz(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 24;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 25 - Default NaN mode control bit"]
    #[inline]
    pub fn clear_dn(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 25);
        self
    }
    #[doc = "Bit 25 - Default NaN mode control bit"]
    #[inline]
    pub fn set_dn(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 25;
        self
    }
    #[doc = "Bit 25 - Default NaN mode control bit"]
    #[inline]
    pub fn dn(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 25;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 26 - Alternative half-precision control bit"]
    #[inline]
    pub fn clear_ahp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 26);
        self
    }
    #[doc = "Bit 26 - Alternative half-precision control bit"]
    #[inline]
    pub fn set_ahp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 26;
        self
    }
    #[doc = "Bit 26 - Alternative half-precision control bit"]
    #[inline]
    pub fn ahp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 26;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 28 - Overflow condition code flag"]
    #[inline]
    pub fn clear_v(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 28);
        self
    }
    #[doc = "Bit 28 - Overflow condition code flag"]
    #[inline]
    pub fn set_v(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 28;
        self
    }
    #[doc = "Bit 28 - Overflow condition code flag"]
    #[inline]
    pub fn v(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 28;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 29 - Carry condition code flag"]
    #[inline]
    pub fn clear_c(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 29);
        self
    }
    #[doc = "Bit 29 - Carry condition code flag"]
    #[inline]
    pub fn set_c(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 29;
        self
    }
    #[doc = "Bit 29 - Carry condition code flag"]
    #[inline]
    pub fn c(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 29;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 30 - Zero condition code flag"]
    #[inline]
    pub fn clear_z(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 30);
        self
    }
    #[doc = "Bit 30 - Zero condition code flag"]
    #[inline]
    pub fn set_z(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 30;
        self
    }
    #[doc = "Bit 30 - Zero condition code flag"]
    #[inline]
    pub fn z(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 30;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 31 - Negative condition code flag"]
    #[inline]
    pub fn clear_n(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 31);
        self
    }
    #[doc = "Bit 31 - Negative condition code flag"]
    #[inline]
    pub fn set_n(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 31;
        self
    }
    #[doc = "Bit 31 - Negative condition code flag"]
    #[inline]
    pub fn n(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 31;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
