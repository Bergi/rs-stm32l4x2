#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::CR2 {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bit 9 - Output Idle state 1"]
    #[inline]
    pub fn ois1n(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 9;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 8 - Output Idle state 1"]
    #[inline]
    pub fn ois1(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 8;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 3 - Capture/compare DMA selection"]
    #[inline]
    pub fn ccds(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 3;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 2 - Capture/compare control update selection"]
    #[inline]
    pub fn ccus(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 0 - Capture/compare preloaded control"]
    #[inline]
    pub fn ccpc(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bit 9 - Output Idle state 1"]
    #[inline]
    pub fn clear_ois1n(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 9);
        self
    }
    #[doc = "Bit 9 - Output Idle state 1"]
    #[inline]
    pub fn set_ois1n(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 9;
        self
    }
    #[doc = "Bit 9 - Output Idle state 1"]
    #[inline]
    pub fn ois1n(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 9;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 8 - Output Idle state 1"]
    #[inline]
    pub fn clear_ois1(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 8);
        self
    }
    #[doc = "Bit 8 - Output Idle state 1"]
    #[inline]
    pub fn set_ois1(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 8;
        self
    }
    #[doc = "Bit 8 - Output Idle state 1"]
    #[inline]
    pub fn ois1(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 8;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 3 - Capture/compare DMA selection"]
    #[inline]
    pub fn clear_ccds(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 3);
        self
    }
    #[doc = "Bit 3 - Capture/compare DMA selection"]
    #[inline]
    pub fn set_ccds(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 3;
        self
    }
    #[doc = "Bit 3 - Capture/compare DMA selection"]
    #[inline]
    pub fn ccds(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 3;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 2 - Capture/compare control update selection"]
    #[inline]
    pub fn clear_ccus(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 2);
        self
    }
    #[doc = "Bit 2 - Capture/compare control update selection"]
    #[inline]
    pub fn set_ccus(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 2;
        self
    }
    #[doc = "Bit 2 - Capture/compare control update selection"]
    #[inline]
    pub fn ccus(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 0 - Capture/compare preloaded control"]
    #[inline]
    pub fn clear_ccpc(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 0);
        self
    }
    #[doc = "Bit 0 - Capture/compare preloaded control"]
    #[inline]
    pub fn set_ccpc(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 0;
        self
    }
    #[doc = "Bit 0 - Capture/compare preloaded control"]
    #[inline]
    pub fn ccpc(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
