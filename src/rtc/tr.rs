#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::TR {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bit 22 - AM/PM notation"]
    #[inline]
    pub fn pm(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 22;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bits 20:21 - Hour tens in BCD format"]
    #[inline]
    pub fn ht(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 20;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 16:19 - Hour units in BCD format"]
    #[inline]
    pub fn hu(&self) -> u8 {
        const MASK: u8 = 15;
        const OFFSET: u8 = 16;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 12:14 - Minute tens in BCD format"]
    #[inline]
    pub fn mnt(&self) -> u8 {
        const MASK: u8 = 7;
        const OFFSET: u8 = 12;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 8:11 - Minute units in BCD format"]
    #[inline]
    pub fn mnu(&self) -> u8 {
        const MASK: u8 = 15;
        const OFFSET: u8 = 8;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 4:6 - Second tens in BCD format"]
    #[inline]
    pub fn st(&self) -> u8 {
        const MASK: u8 = 7;
        const OFFSET: u8 = 4;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 0:3 - Second units in BCD format"]
    #[inline]
    pub fn su(&self) -> u8 {
        const MASK: u8 = 15;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bit 22 - AM/PM notation"]
    #[inline]
    pub fn clear_pm(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 22);
        self
    }
    #[doc = "Bit 22 - AM/PM notation"]
    #[inline]
    pub fn set_pm(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 22;
        self
    }
    #[doc = "Bit 22 - AM/PM notation"]
    #[inline]
    pub fn pm(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 22;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 20:21 - Hour tens in BCD format"]
    #[inline]
    pub unsafe fn ht(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 20;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 16:19 - Hour units in BCD format"]
    #[inline]
    pub unsafe fn hu(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 15;
        const OFFSET: u8 = 16;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 12:14 - Minute tens in BCD format"]
    #[inline]
    pub unsafe fn mnt(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 7;
        const OFFSET: u8 = 12;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 8:11 - Minute units in BCD format"]
    #[inline]
    pub unsafe fn mnu(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 15;
        const OFFSET: u8 = 8;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 4:6 - Second tens in BCD format"]
    #[inline]
    pub unsafe fn st(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 7;
        const OFFSET: u8 = 4;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 0:3 - Second units in BCD format"]
    #[inline]
    pub unsafe fn su(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 15;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
