#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
impl super::TSDR {
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bits 13:15 - Week day units"]
    #[inline]
    pub fn wdu(&self) -> u8 {
        const MASK: u8 = 7;
        const OFFSET: u8 = 13;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bit 12 - Month tens in BCD format"]
    #[inline]
    pub fn mt(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 12;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bits 8:11 - Month units in BCD format"]
    #[inline]
    pub fn mu(&self) -> u8 {
        const MASK: u8 = 15;
        const OFFSET: u8 = 8;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 4:5 - Date tens in BCD format"]
    #[inline]
    pub fn dt(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 4;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 0:3 - Date units in BCD format"]
    #[inline]
    pub fn du(&self) -> u8 {
        const MASK: u8 = 15;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
}
