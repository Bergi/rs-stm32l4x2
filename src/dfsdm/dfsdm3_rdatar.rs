#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
impl super::DFSDM3_RDATAR {
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bits 8:31 - Regular channel conversion data"]
    #[inline]
    pub fn rdata(&self) -> u32 {
        const MASK: u32 = 16777215;
        const OFFSET: u8 = 8;
        ((self.bits >> OFFSET) & MASK as u32) as u32
    }
    #[doc = "Bit 4 - Regular channel pending data"]
    #[inline]
    pub fn rpend(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 4;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bits 0:2 - Regular channel most recently converted"]
    #[inline]
    pub fn rdatach(&self) -> u8 {
        const MASK: u8 = 7;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
}
