#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::CR {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bits 8:13 - HSI48 oscillator smooth trimming"]
    #[inline]
    pub fn trim(&self) -> u8 {
        const MASK: u8 = 63;
        const OFFSET: u8 = 8;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bit 7 - Generate software SYNC event"]
    #[inline]
    pub fn swsync(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 7;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 6 - Automatic trimming enable"]
    #[inline]
    pub fn autotrimen(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 6;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 5 - Frequency error counter enable"]
    #[inline]
    pub fn cen(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 5;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 3 - Expected SYNC interrupt enable"]
    #[inline]
    pub fn esyncie(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 3;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 2 - Synchronization or trimming error interrupt enable"]
    #[inline]
    pub fn errie(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 1 - SYNC warning interrupt enable"]
    #[inline]
    pub fn syncwarnie(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 0 - SYNC event OK interrupt enable"]
    #[inline]
    pub fn syncokie(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 8192 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bits 8:13 - HSI48 oscillator smooth trimming"]
    #[inline]
    pub unsafe fn trim(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 63;
        const OFFSET: u8 = 8;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 7 - Generate software SYNC event"]
    #[inline]
    pub fn clear_swsync(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 7);
        self
    }
    #[doc = "Bit 7 - Generate software SYNC event"]
    #[inline]
    pub fn set_swsync(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 7;
        self
    }
    #[doc = "Bit 7 - Generate software SYNC event"]
    #[inline]
    pub fn swsync(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 7;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 6 - Automatic trimming enable"]
    #[inline]
    pub fn clear_autotrimen(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 6);
        self
    }
    #[doc = "Bit 6 - Automatic trimming enable"]
    #[inline]
    pub fn set_autotrimen(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 6;
        self
    }
    #[doc = "Bit 6 - Automatic trimming enable"]
    #[inline]
    pub fn autotrimen(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 6;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 5 - Frequency error counter enable"]
    #[inline]
    pub fn clear_cen(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 5);
        self
    }
    #[doc = "Bit 5 - Frequency error counter enable"]
    #[inline]
    pub fn set_cen(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 5;
        self
    }
    #[doc = "Bit 5 - Frequency error counter enable"]
    #[inline]
    pub fn cen(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 5;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 3 - Expected SYNC interrupt enable"]
    #[inline]
    pub fn clear_esyncie(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 3);
        self
    }
    #[doc = "Bit 3 - Expected SYNC interrupt enable"]
    #[inline]
    pub fn set_esyncie(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 3;
        self
    }
    #[doc = "Bit 3 - Expected SYNC interrupt enable"]
    #[inline]
    pub fn esyncie(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 3;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 2 - Synchronization or trimming error interrupt enable"]
    #[inline]
    pub fn clear_errie(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 2);
        self
    }
    #[doc = "Bit 2 - Synchronization or trimming error interrupt enable"]
    #[inline]
    pub fn set_errie(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 2;
        self
    }
    #[doc = "Bit 2 - Synchronization or trimming error interrupt enable"]
    #[inline]
    pub fn errie(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 1 - SYNC warning interrupt enable"]
    #[inline]
    pub fn clear_syncwarnie(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 1);
        self
    }
    #[doc = "Bit 1 - SYNC warning interrupt enable"]
    #[inline]
    pub fn set_syncwarnie(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 1;
        self
    }
    #[doc = "Bit 1 - SYNC warning interrupt enable"]
    #[inline]
    pub fn syncwarnie(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 0 - SYNC event OK interrupt enable"]
    #[inline]
    pub fn clear_syncokie(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 0);
        self
    }
    #[doc = "Bit 0 - SYNC event OK interrupt enable"]
    #[inline]
    pub fn set_syncokie(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 0;
        self
    }
    #[doc = "Bit 0 - SYNC event OK interrupt enable"]
    #[inline]
    pub fn syncokie(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
