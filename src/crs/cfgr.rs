#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::CFGR {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bit 31 - SYNC polarity selection"]
    #[inline]
    pub fn syncpol(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 31;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bits 28:29 - SYNC signal source selection"]
    #[inline]
    pub fn syncsrc(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 28;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 24:26 - SYNC divider"]
    #[inline]
    pub fn syncdiv(&self) -> u8 {
        const MASK: u8 = 7;
        const OFFSET: u8 = 24;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 16:23 - Frequency error limit"]
    #[inline]
    pub fn felim(&self) -> u8 {
        const MASK: u8 = 255;
        const OFFSET: u8 = 16;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 0:15 - Counter reload value"]
    #[inline]
    pub fn reload(&self) -> u16 {
        const MASK: u16 = 65535;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) as u16
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 539147135 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bit 31 - SYNC polarity selection"]
    #[inline]
    pub fn clear_syncpol(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 31);
        self
    }
    #[doc = "Bit 31 - SYNC polarity selection"]
    #[inline]
    pub fn set_syncpol(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 31;
        self
    }
    #[doc = "Bit 31 - SYNC polarity selection"]
    #[inline]
    pub fn syncpol(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 31;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 28:29 - SYNC signal source selection"]
    #[inline]
    pub unsafe fn syncsrc(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 28;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 24:26 - SYNC divider"]
    #[inline]
    pub unsafe fn syncdiv(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 7;
        const OFFSET: u8 = 24;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 16:23 - Frequency error limit"]
    #[inline]
    pub unsafe fn felim(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 255;
        const OFFSET: u8 = 16;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 0:15 - Counter reload value"]
    #[inline]
    pub unsafe fn reload(&mut self, value: u16) -> &mut Self {
        const MASK: u16 = 65535;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
