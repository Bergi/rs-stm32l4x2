#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
impl super::ISR {
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bits 16:31 - Frequency error capture"]
    #[inline]
    pub fn fecap(&self) -> u16 {
        const MASK: u16 = 65535;
        const OFFSET: u8 = 16;
        ((self.bits >> OFFSET) & MASK as u32) as u16
    }
    #[doc = "Bit 15 - Frequency error direction"]
    #[inline]
    pub fn fedir(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 15;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 10 - Trimming overflow or underflow"]
    #[inline]
    pub fn trimovf(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 10;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 9 - SYNC missed"]
    #[inline]
    pub fn syncmiss(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 9;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 8 - SYNC error"]
    #[inline]
    pub fn syncerr(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 8;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 3 - Expected SYNC flag"]
    #[inline]
    pub fn esyncf(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 3;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 2 - Error flag"]
    #[inline]
    pub fn errf(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 1 - SYNC warning flag"]
    #[inline]
    pub fn syncwarnf(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 0 - SYNC event OK flag"]
    #[inline]
    pub fn syncokf(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
}
