#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::OPTR {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bits 0:7 - Read protection level"]
    #[inline]
    pub fn rdp(&self) -> u8 {
        const MASK: u8 = 255;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 8:10 - BOR reset Level"]
    #[inline]
    pub fn bor_lev(&self) -> u8 {
        const MASK: u8 = 7;
        const OFFSET: u8 = 8;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bit 12 - nRST_STOP"]
    #[inline]
    pub fn n_rst_stop(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 12;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 13 - nRST_STDBY"]
    #[inline]
    pub fn n_rst_stdby(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 13;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 16 - Independent watchdog selection"]
    #[inline]
    pub fn idwg_sw(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 16;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 17 - Independent watchdog counter freeze in Stop mode"]
    #[inline]
    pub fn iwdg_stop(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 17;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 18 - Independent watchdog counter freeze in Standby mode"]
    #[inline]
    pub fn iwdg_stdby(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 18;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 19 - Window watchdog selection"]
    #[inline]
    pub fn wwdg_sw(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 19;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 20 - Dual-bank boot"]
    #[inline]
    pub fn bfb2(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 20;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 21 - Dual-Bank on 512 KB or 256 KB Flash memory devices"]
    #[inline]
    pub fn dualbank(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 21;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 23 - Boot configuration"]
    #[inline]
    pub fn n_boot1(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 23;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 24 - SRAM2 parity check enable"]
    #[inline]
    pub fn sram2_pe(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 24;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 25 - SRAM2 Erase when system reset"]
    #[inline]
    pub fn sram2_rst(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 25;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 4026531840 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bits 0:7 - Read protection level"]
    #[inline]
    pub unsafe fn rdp(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 255;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 8:10 - BOR reset Level"]
    #[inline]
    pub unsafe fn bor_lev(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 7;
        const OFFSET: u8 = 8;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 12 - nRST_STOP"]
    #[inline]
    pub fn clear_n_rst_stop(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 12);
        self
    }
    #[doc = "Bit 12 - nRST_STOP"]
    #[inline]
    pub fn set_n_rst_stop(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 12;
        self
    }
    #[doc = "Bit 12 - nRST_STOP"]
    #[inline]
    pub fn n_rst_stop(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 12;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 13 - nRST_STDBY"]
    #[inline]
    pub fn clear_n_rst_stdby(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 13);
        self
    }
    #[doc = "Bit 13 - nRST_STDBY"]
    #[inline]
    pub fn set_n_rst_stdby(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 13;
        self
    }
    #[doc = "Bit 13 - nRST_STDBY"]
    #[inline]
    pub fn n_rst_stdby(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 13;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 16 - Independent watchdog selection"]
    #[inline]
    pub fn clear_idwg_sw(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 16);
        self
    }
    #[doc = "Bit 16 - Independent watchdog selection"]
    #[inline]
    pub fn set_idwg_sw(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 16;
        self
    }
    #[doc = "Bit 16 - Independent watchdog selection"]
    #[inline]
    pub fn idwg_sw(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 16;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 17 - Independent watchdog counter freeze in Stop mode"]
    #[inline]
    pub fn clear_iwdg_stop(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 17);
        self
    }
    #[doc = "Bit 17 - Independent watchdog counter freeze in Stop mode"]
    #[inline]
    pub fn set_iwdg_stop(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 17;
        self
    }
    #[doc = "Bit 17 - Independent watchdog counter freeze in Stop mode"]
    #[inline]
    pub fn iwdg_stop(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 17;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 18 - Independent watchdog counter freeze in Standby mode"]
    #[inline]
    pub fn clear_iwdg_stdby(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 18);
        self
    }
    #[doc = "Bit 18 - Independent watchdog counter freeze in Standby mode"]
    #[inline]
    pub fn set_iwdg_stdby(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 18;
        self
    }
    #[doc = "Bit 18 - Independent watchdog counter freeze in Standby mode"]
    #[inline]
    pub fn iwdg_stdby(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 18;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 19 - Window watchdog selection"]
    #[inline]
    pub fn clear_wwdg_sw(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 19);
        self
    }
    #[doc = "Bit 19 - Window watchdog selection"]
    #[inline]
    pub fn set_wwdg_sw(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 19;
        self
    }
    #[doc = "Bit 19 - Window watchdog selection"]
    #[inline]
    pub fn wwdg_sw(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 19;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 20 - Dual-bank boot"]
    #[inline]
    pub fn clear_bfb2(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 20);
        self
    }
    #[doc = "Bit 20 - Dual-bank boot"]
    #[inline]
    pub fn set_bfb2(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 20;
        self
    }
    #[doc = "Bit 20 - Dual-bank boot"]
    #[inline]
    pub fn bfb2(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 20;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 21 - Dual-Bank on 512 KB or 256 KB Flash memory devices"]
    #[inline]
    pub fn clear_dualbank(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 21);
        self
    }
    #[doc = "Bit 21 - Dual-Bank on 512 KB or 256 KB Flash memory devices"]
    #[inline]
    pub fn set_dualbank(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 21;
        self
    }
    #[doc = "Bit 21 - Dual-Bank on 512 KB or 256 KB Flash memory devices"]
    #[inline]
    pub fn dualbank(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 21;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 23 - Boot configuration"]
    #[inline]
    pub fn clear_n_boot1(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 23);
        self
    }
    #[doc = "Bit 23 - Boot configuration"]
    #[inline]
    pub fn set_n_boot1(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 23;
        self
    }
    #[doc = "Bit 23 - Boot configuration"]
    #[inline]
    pub fn n_boot1(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 23;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 24 - SRAM2 parity check enable"]
    #[inline]
    pub fn clear_sram2_pe(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 24);
        self
    }
    #[doc = "Bit 24 - SRAM2 parity check enable"]
    #[inline]
    pub fn set_sram2_pe(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 24;
        self
    }
    #[doc = "Bit 24 - SRAM2 parity check enable"]
    #[inline]
    pub fn sram2_pe(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 24;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 25 - SRAM2 Erase when system reset"]
    #[inline]
    pub fn clear_sram2_rst(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 25);
        self
    }
    #[doc = "Bit 25 - SRAM2 Erase when system reset"]
    #[inline]
    pub fn set_sram2_rst(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 25;
        self
    }
    #[doc = "Bit 25 - SRAM2 Erase when system reset"]
    #[inline]
    pub fn sram2_rst(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 25;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
