#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::CR {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bit 0 - Programming"]
    #[inline]
    pub fn pg(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 1 - Page erase"]
    #[inline]
    pub fn per(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 2 - Bank 1 Mass erase"]
    #[inline]
    pub fn mer1(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bits 3:10 - Page number"]
    #[inline]
    pub fn pnb(&self) -> u8 {
        const MASK: u8 = 255;
        const OFFSET: u8 = 3;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bit 11 - Bank erase"]
    #[inline]
    pub fn bker(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 11;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 15 - Bank 2 Mass erase"]
    #[inline]
    pub fn mer2(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 15;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 16 - Start"]
    #[inline]
    pub fn start(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 16;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 17 - Options modification start"]
    #[inline]
    pub fn optstrt(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 17;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 18 - Fast programming"]
    #[inline]
    pub fn fstpg(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 18;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 24 - End of operation interrupt enable"]
    #[inline]
    pub fn eopie(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 24;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 25 - Error interrupt enable"]
    #[inline]
    pub fn errie(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 25;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 26 - PCROP read error interrupt enable"]
    #[inline]
    pub fn rderrie(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 26;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 27 - Force the option byte loading"]
    #[inline]
    pub fn obl_launch(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 27;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 30 - Options Lock"]
    #[inline]
    pub fn optlock(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 30;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 31 - FLASH_CR Lock"]
    #[inline]
    pub fn lock(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 31;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 3221225472 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bit 0 - Programming"]
    #[inline]
    pub fn clear_pg(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 0);
        self
    }
    #[doc = "Bit 0 - Programming"]
    #[inline]
    pub fn set_pg(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 0;
        self
    }
    #[doc = "Bit 0 - Programming"]
    #[inline]
    pub fn pg(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 1 - Page erase"]
    #[inline]
    pub fn clear_per(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 1);
        self
    }
    #[doc = "Bit 1 - Page erase"]
    #[inline]
    pub fn set_per(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 1;
        self
    }
    #[doc = "Bit 1 - Page erase"]
    #[inline]
    pub fn per(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 2 - Bank 1 Mass erase"]
    #[inline]
    pub fn clear_mer1(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 2);
        self
    }
    #[doc = "Bit 2 - Bank 1 Mass erase"]
    #[inline]
    pub fn set_mer1(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 2;
        self
    }
    #[doc = "Bit 2 - Bank 1 Mass erase"]
    #[inline]
    pub fn mer1(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 3:10 - Page number"]
    #[inline]
    pub unsafe fn pnb(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 255;
        const OFFSET: u8 = 3;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 11 - Bank erase"]
    #[inline]
    pub fn clear_bker(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 11);
        self
    }
    #[doc = "Bit 11 - Bank erase"]
    #[inline]
    pub fn set_bker(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 11;
        self
    }
    #[doc = "Bit 11 - Bank erase"]
    #[inline]
    pub fn bker(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 11;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 15 - Bank 2 Mass erase"]
    #[inline]
    pub fn clear_mer2(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 15);
        self
    }
    #[doc = "Bit 15 - Bank 2 Mass erase"]
    #[inline]
    pub fn set_mer2(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 15;
        self
    }
    #[doc = "Bit 15 - Bank 2 Mass erase"]
    #[inline]
    pub fn mer2(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 15;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 16 - Start"]
    #[inline]
    pub fn clear_start(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 16);
        self
    }
    #[doc = "Bit 16 - Start"]
    #[inline]
    pub fn set_start(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 16;
        self
    }
    #[doc = "Bit 16 - Start"]
    #[inline]
    pub fn start(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 16;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 17 - Options modification start"]
    #[inline]
    pub fn clear_optstrt(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 17);
        self
    }
    #[doc = "Bit 17 - Options modification start"]
    #[inline]
    pub fn set_optstrt(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 17;
        self
    }
    #[doc = "Bit 17 - Options modification start"]
    #[inline]
    pub fn optstrt(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 17;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 18 - Fast programming"]
    #[inline]
    pub fn clear_fstpg(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 18);
        self
    }
    #[doc = "Bit 18 - Fast programming"]
    #[inline]
    pub fn set_fstpg(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 18;
        self
    }
    #[doc = "Bit 18 - Fast programming"]
    #[inline]
    pub fn fstpg(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 18;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 24 - End of operation interrupt enable"]
    #[inline]
    pub fn clear_eopie(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 24);
        self
    }
    #[doc = "Bit 24 - End of operation interrupt enable"]
    #[inline]
    pub fn set_eopie(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 24;
        self
    }
    #[doc = "Bit 24 - End of operation interrupt enable"]
    #[inline]
    pub fn eopie(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 24;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 25 - Error interrupt enable"]
    #[inline]
    pub fn clear_errie(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 25);
        self
    }
    #[doc = "Bit 25 - Error interrupt enable"]
    #[inline]
    pub fn set_errie(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 25;
        self
    }
    #[doc = "Bit 25 - Error interrupt enable"]
    #[inline]
    pub fn errie(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 25;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 26 - PCROP read error interrupt enable"]
    #[inline]
    pub fn clear_rderrie(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 26);
        self
    }
    #[doc = "Bit 26 - PCROP read error interrupt enable"]
    #[inline]
    pub fn set_rderrie(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 26;
        self
    }
    #[doc = "Bit 26 - PCROP read error interrupt enable"]
    #[inline]
    pub fn rderrie(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 26;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 27 - Force the option byte loading"]
    #[inline]
    pub fn clear_obl_launch(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 27);
        self
    }
    #[doc = "Bit 27 - Force the option byte loading"]
    #[inline]
    pub fn set_obl_launch(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 27;
        self
    }
    #[doc = "Bit 27 - Force the option byte loading"]
    #[inline]
    pub fn obl_launch(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 27;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 30 - Options Lock"]
    #[inline]
    pub fn clear_optlock(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 30);
        self
    }
    #[doc = "Bit 30 - Options Lock"]
    #[inline]
    pub fn set_optlock(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 30;
        self
    }
    #[doc = "Bit 30 - Options Lock"]
    #[inline]
    pub fn optlock(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 30;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 31 - FLASH_CR Lock"]
    #[inline]
    pub fn clear_lock(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 31);
        self
    }
    #[doc = "Bit 31 - FLASH_CR Lock"]
    #[inline]
    pub fn set_lock(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 31;
        self
    }
    #[doc = "Bit 31 - FLASH_CR Lock"]
    #[inline]
    pub fn lock(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 31;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
