#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::CMD {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bit 14 - CE-ATA command"]
    #[inline]
    pub fn ce_atacmd(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 14;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 13 - not Interrupt Enable"]
    #[inline]
    pub fn n_ien(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 13;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 12 - Enable CMD completion"]
    #[inline]
    pub fn encmdcompl(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 12;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 11 - SD I/O suspend command"]
    #[inline]
    pub fn sdiosuspend(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 11;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 10 - Command path state machine (CPSM) Enable bit"]
    #[inline]
    pub fn cpsmen(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 10;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 9 - CPSM Waits for ends of data transfer (CmdPend internal signal)"]
    #[inline]
    pub fn waitpend(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 9;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 8 - CPSM waits for interrupt request"]
    #[inline]
    pub fn waitint(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 8;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bits 6:7 - Wait for response bits"]
    #[inline]
    pub fn waitresp(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 6;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 0:5 - Command index"]
    #[inline]
    pub fn cmdindex(&self) -> u8 {
        const MASK: u8 = 63;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bit 14 - CE-ATA command"]
    #[inline]
    pub fn clear_ce_atacmd(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 14);
        self
    }
    #[doc = "Bit 14 - CE-ATA command"]
    #[inline]
    pub fn set_ce_atacmd(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 14;
        self
    }
    #[doc = "Bit 14 - CE-ATA command"]
    #[inline]
    pub fn ce_atacmd(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 14;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 13 - not Interrupt Enable"]
    #[inline]
    pub fn clear_n_ien(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 13);
        self
    }
    #[doc = "Bit 13 - not Interrupt Enable"]
    #[inline]
    pub fn set_n_ien(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 13;
        self
    }
    #[doc = "Bit 13 - not Interrupt Enable"]
    #[inline]
    pub fn n_ien(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 13;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 12 - Enable CMD completion"]
    #[inline]
    pub fn clear_encmdcompl(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 12);
        self
    }
    #[doc = "Bit 12 - Enable CMD completion"]
    #[inline]
    pub fn set_encmdcompl(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 12;
        self
    }
    #[doc = "Bit 12 - Enable CMD completion"]
    #[inline]
    pub fn encmdcompl(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 12;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 11 - SD I/O suspend command"]
    #[inline]
    pub fn clear_sdiosuspend(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 11);
        self
    }
    #[doc = "Bit 11 - SD I/O suspend command"]
    #[inline]
    pub fn set_sdiosuspend(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 11;
        self
    }
    #[doc = "Bit 11 - SD I/O suspend command"]
    #[inline]
    pub fn sdiosuspend(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 11;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 10 - Command path state machine (CPSM) Enable bit"]
    #[inline]
    pub fn clear_cpsmen(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 10);
        self
    }
    #[doc = "Bit 10 - Command path state machine (CPSM) Enable bit"]
    #[inline]
    pub fn set_cpsmen(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 10;
        self
    }
    #[doc = "Bit 10 - Command path state machine (CPSM) Enable bit"]
    #[inline]
    pub fn cpsmen(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 10;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 9 - CPSM Waits for ends of data transfer (CmdPend internal signal)"]
    #[inline]
    pub fn clear_waitpend(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 9);
        self
    }
    #[doc = "Bit 9 - CPSM Waits for ends of data transfer (CmdPend internal signal)"]
    #[inline]
    pub fn set_waitpend(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 9;
        self
    }
    #[doc = "Bit 9 - CPSM Waits for ends of data transfer (CmdPend internal signal)"]
    #[inline]
    pub fn waitpend(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 9;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 8 - CPSM waits for interrupt request"]
    #[inline]
    pub fn clear_waitint(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 8);
        self
    }
    #[doc = "Bit 8 - CPSM waits for interrupt request"]
    #[inline]
    pub fn set_waitint(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 8;
        self
    }
    #[doc = "Bit 8 - CPSM waits for interrupt request"]
    #[inline]
    pub fn waitint(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 8;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 6:7 - Wait for response bits"]
    #[inline]
    pub unsafe fn waitresp(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 6;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 0:5 - Command index"]
    #[inline]
    pub unsafe fn cmdindex(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 63;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
