#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::SR {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bit 13 - DAC channel1 DMA underrun flag"]
    #[inline]
    pub fn dmaudr1(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 13;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 14 - DAC Channel 1 calibration offset status"]
    #[inline]
    pub fn cal_flag1(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 14;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 15 - DAC Channel 1 busy writing sample time flag"]
    #[inline]
    pub fn bwst1(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 15;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 29 - DAC channel2 DMA underrun flag"]
    #[inline]
    pub fn dmaudr2(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 29;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 30 - DAC Channel 2 calibration offset status"]
    #[inline]
    pub fn cal_flag2(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 30;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 31 - DAC Channel 2 busy writing sample time flag"]
    #[inline]
    pub fn bwst2(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 31;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bit 13 - DAC channel1 DMA underrun flag"]
    #[inline]
    pub fn clear_dmaudr1(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 13);
        self
    }
    #[doc = "Bit 13 - DAC channel1 DMA underrun flag"]
    #[inline]
    pub fn set_dmaudr1(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 13;
        self
    }
    #[doc = "Bit 13 - DAC channel1 DMA underrun flag"]
    #[inline]
    pub fn dmaudr1(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 13;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 29 - DAC channel2 DMA underrun flag"]
    #[inline]
    pub fn clear_dmaudr2(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 29);
        self
    }
    #[doc = "Bit 29 - DAC channel2 DMA underrun flag"]
    #[inline]
    pub fn set_dmaudr2(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 29;
        self
    }
    #[doc = "Bit 29 - DAC channel2 DMA underrun flag"]
    #[inline]
    pub fn dmaudr2(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 29;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
