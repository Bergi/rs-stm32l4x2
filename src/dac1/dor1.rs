#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
impl super::DOR1 {
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bits 0:11 - DAC channel1 data output"]
    #[inline]
    pub fn dacc1dor(&self) -> u16 {
        const MASK: u16 = 4095;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) as u16
    }
}
