#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::SWPR {
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bit 31 - SRAM2 page 31 write protection"]
    #[inline]
    pub fn clear_p31wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 31);
        self
    }
    #[doc = "Bit 31 - SRAM2 page 31 write protection"]
    #[inline]
    pub fn set_p31wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 31;
        self
    }
    #[doc = "Bit 31 - SRAM2 page 31 write protection"]
    #[inline]
    pub fn p31wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 31;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 30 - P30WP"]
    #[inline]
    pub fn clear_p30wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 30);
        self
    }
    #[doc = "Bit 30 - P30WP"]
    #[inline]
    pub fn set_p30wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 30;
        self
    }
    #[doc = "Bit 30 - P30WP"]
    #[inline]
    pub fn p30wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 30;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 29 - P29WP"]
    #[inline]
    pub fn clear_p29wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 29);
        self
    }
    #[doc = "Bit 29 - P29WP"]
    #[inline]
    pub fn set_p29wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 29;
        self
    }
    #[doc = "Bit 29 - P29WP"]
    #[inline]
    pub fn p29wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 29;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 28 - P28WP"]
    #[inline]
    pub fn clear_p28wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 28);
        self
    }
    #[doc = "Bit 28 - P28WP"]
    #[inline]
    pub fn set_p28wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 28;
        self
    }
    #[doc = "Bit 28 - P28WP"]
    #[inline]
    pub fn p28wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 28;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 27 - P27WP"]
    #[inline]
    pub fn clear_p27wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 27);
        self
    }
    #[doc = "Bit 27 - P27WP"]
    #[inline]
    pub fn set_p27wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 27;
        self
    }
    #[doc = "Bit 27 - P27WP"]
    #[inline]
    pub fn p27wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 27;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 26 - P26WP"]
    #[inline]
    pub fn clear_p26wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 26);
        self
    }
    #[doc = "Bit 26 - P26WP"]
    #[inline]
    pub fn set_p26wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 26;
        self
    }
    #[doc = "Bit 26 - P26WP"]
    #[inline]
    pub fn p26wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 26;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 25 - P25WP"]
    #[inline]
    pub fn clear_p25wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 25);
        self
    }
    #[doc = "Bit 25 - P25WP"]
    #[inline]
    pub fn set_p25wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 25;
        self
    }
    #[doc = "Bit 25 - P25WP"]
    #[inline]
    pub fn p25wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 25;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 24 - P24WP"]
    #[inline]
    pub fn clear_p24wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 24);
        self
    }
    #[doc = "Bit 24 - P24WP"]
    #[inline]
    pub fn set_p24wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 24;
        self
    }
    #[doc = "Bit 24 - P24WP"]
    #[inline]
    pub fn p24wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 24;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 23 - P23WP"]
    #[inline]
    pub fn clear_p23wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 23);
        self
    }
    #[doc = "Bit 23 - P23WP"]
    #[inline]
    pub fn set_p23wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 23;
        self
    }
    #[doc = "Bit 23 - P23WP"]
    #[inline]
    pub fn p23wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 23;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 22 - P22WP"]
    #[inline]
    pub fn clear_p22wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 22);
        self
    }
    #[doc = "Bit 22 - P22WP"]
    #[inline]
    pub fn set_p22wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 22;
        self
    }
    #[doc = "Bit 22 - P22WP"]
    #[inline]
    pub fn p22wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 22;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 21 - P21WP"]
    #[inline]
    pub fn clear_p21wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 21);
        self
    }
    #[doc = "Bit 21 - P21WP"]
    #[inline]
    pub fn set_p21wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 21;
        self
    }
    #[doc = "Bit 21 - P21WP"]
    #[inline]
    pub fn p21wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 21;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 20 - P20WP"]
    #[inline]
    pub fn clear_p20wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 20);
        self
    }
    #[doc = "Bit 20 - P20WP"]
    #[inline]
    pub fn set_p20wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 20;
        self
    }
    #[doc = "Bit 20 - P20WP"]
    #[inline]
    pub fn p20wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 20;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 19 - P19WP"]
    #[inline]
    pub fn clear_p19wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 19);
        self
    }
    #[doc = "Bit 19 - P19WP"]
    #[inline]
    pub fn set_p19wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 19;
        self
    }
    #[doc = "Bit 19 - P19WP"]
    #[inline]
    pub fn p19wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 19;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 18 - P18WP"]
    #[inline]
    pub fn clear_p18wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 18);
        self
    }
    #[doc = "Bit 18 - P18WP"]
    #[inline]
    pub fn set_p18wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 18;
        self
    }
    #[doc = "Bit 18 - P18WP"]
    #[inline]
    pub fn p18wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 18;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 17 - P17WP"]
    #[inline]
    pub fn clear_p17wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 17);
        self
    }
    #[doc = "Bit 17 - P17WP"]
    #[inline]
    pub fn set_p17wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 17;
        self
    }
    #[doc = "Bit 17 - P17WP"]
    #[inline]
    pub fn p17wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 17;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 16 - P16WP"]
    #[inline]
    pub fn clear_p16wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 16);
        self
    }
    #[doc = "Bit 16 - P16WP"]
    #[inline]
    pub fn set_p16wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 16;
        self
    }
    #[doc = "Bit 16 - P16WP"]
    #[inline]
    pub fn p16wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 16;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 15 - P15WP"]
    #[inline]
    pub fn clear_p15wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 15);
        self
    }
    #[doc = "Bit 15 - P15WP"]
    #[inline]
    pub fn set_p15wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 15;
        self
    }
    #[doc = "Bit 15 - P15WP"]
    #[inline]
    pub fn p15wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 15;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 14 - P14WP"]
    #[inline]
    pub fn clear_p14wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 14);
        self
    }
    #[doc = "Bit 14 - P14WP"]
    #[inline]
    pub fn set_p14wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 14;
        self
    }
    #[doc = "Bit 14 - P14WP"]
    #[inline]
    pub fn p14wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 14;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 13 - P13WP"]
    #[inline]
    pub fn clear_p13wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 13);
        self
    }
    #[doc = "Bit 13 - P13WP"]
    #[inline]
    pub fn set_p13wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 13;
        self
    }
    #[doc = "Bit 13 - P13WP"]
    #[inline]
    pub fn p13wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 13;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 12 - P12WP"]
    #[inline]
    pub fn clear_p12wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 12);
        self
    }
    #[doc = "Bit 12 - P12WP"]
    #[inline]
    pub fn set_p12wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 12;
        self
    }
    #[doc = "Bit 12 - P12WP"]
    #[inline]
    pub fn p12wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 12;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 11 - P11WP"]
    #[inline]
    pub fn clear_p11wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 11);
        self
    }
    #[doc = "Bit 11 - P11WP"]
    #[inline]
    pub fn set_p11wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 11;
        self
    }
    #[doc = "Bit 11 - P11WP"]
    #[inline]
    pub fn p11wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 11;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 10 - P10WP"]
    #[inline]
    pub fn clear_p10wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 10);
        self
    }
    #[doc = "Bit 10 - P10WP"]
    #[inline]
    pub fn set_p10wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 10;
        self
    }
    #[doc = "Bit 10 - P10WP"]
    #[inline]
    pub fn p10wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 10;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 9 - P9WP"]
    #[inline]
    pub fn clear_p9wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 9);
        self
    }
    #[doc = "Bit 9 - P9WP"]
    #[inline]
    pub fn set_p9wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 9;
        self
    }
    #[doc = "Bit 9 - P9WP"]
    #[inline]
    pub fn p9wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 9;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 8 - P8WP"]
    #[inline]
    pub fn clear_p8wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 8);
        self
    }
    #[doc = "Bit 8 - P8WP"]
    #[inline]
    pub fn set_p8wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 8;
        self
    }
    #[doc = "Bit 8 - P8WP"]
    #[inline]
    pub fn p8wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 8;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 7 - P7WP"]
    #[inline]
    pub fn clear_p7wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 7);
        self
    }
    #[doc = "Bit 7 - P7WP"]
    #[inline]
    pub fn set_p7wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 7;
        self
    }
    #[doc = "Bit 7 - P7WP"]
    #[inline]
    pub fn p7wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 7;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 6 - P6WP"]
    #[inline]
    pub fn clear_p6wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 6);
        self
    }
    #[doc = "Bit 6 - P6WP"]
    #[inline]
    pub fn set_p6wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 6;
        self
    }
    #[doc = "Bit 6 - P6WP"]
    #[inline]
    pub fn p6wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 6;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 5 - P5WP"]
    #[inline]
    pub fn clear_p5wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 5);
        self
    }
    #[doc = "Bit 5 - P5WP"]
    #[inline]
    pub fn set_p5wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 5;
        self
    }
    #[doc = "Bit 5 - P5WP"]
    #[inline]
    pub fn p5wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 5;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 4 - P4WP"]
    #[inline]
    pub fn clear_p4wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 4);
        self
    }
    #[doc = "Bit 4 - P4WP"]
    #[inline]
    pub fn set_p4wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 4;
        self
    }
    #[doc = "Bit 4 - P4WP"]
    #[inline]
    pub fn p4wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 4;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 3 - P3WP"]
    #[inline]
    pub fn clear_p3wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 3);
        self
    }
    #[doc = "Bit 3 - P3WP"]
    #[inline]
    pub fn set_p3wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 3;
        self
    }
    #[doc = "Bit 3 - P3WP"]
    #[inline]
    pub fn p3wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 3;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 2 - P2WP"]
    #[inline]
    pub fn clear_p2wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 2);
        self
    }
    #[doc = "Bit 2 - P2WP"]
    #[inline]
    pub fn set_p2wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 2;
        self
    }
    #[doc = "Bit 2 - P2WP"]
    #[inline]
    pub fn p2wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 1 - P1WP"]
    #[inline]
    pub fn clear_p1wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 1);
        self
    }
    #[doc = "Bit 1 - P1WP"]
    #[inline]
    pub fn set_p1wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 1;
        self
    }
    #[doc = "Bit 1 - P1WP"]
    #[inline]
    pub fn p1wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 0 - P0WP"]
    #[inline]
    pub fn clear_p0wp(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 0);
        self
    }
    #[doc = "Bit 0 - P0WP"]
    #[inline]
    pub fn set_p0wp(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 0;
        self
    }
    #[doc = "Bit 0 - P0WP"]
    #[inline]
    pub fn p0wp(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
