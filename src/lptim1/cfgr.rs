#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::CFGR {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bit 24 - Encoder mode enable"]
    #[inline]
    pub fn enc(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 24;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 23 - counter mode enabled"]
    #[inline]
    pub fn countmode(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 23;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 22 - Registers update mode"]
    #[inline]
    pub fn preload(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 22;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 21 - Waveform shape polarity"]
    #[inline]
    pub fn wavpol(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 21;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 20 - Waveform shape"]
    #[inline]
    pub fn wave(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 20;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 19 - Timeout enable"]
    #[inline]
    pub fn timout(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 19;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bits 17:18 - Trigger enable and polarity"]
    #[inline]
    pub fn trigen(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 17;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 13:15 - Trigger selector"]
    #[inline]
    pub fn trigsel(&self) -> u8 {
        const MASK: u8 = 7;
        const OFFSET: u8 = 13;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 9:11 - Clock prescaler"]
    #[inline]
    pub fn presc(&self) -> u8 {
        const MASK: u8 = 7;
        const OFFSET: u8 = 9;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 6:7 - Configurable digital filter for trigger"]
    #[inline]
    pub fn trgflt(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 6;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 3:4 - Configurable digital filter for external clock"]
    #[inline]
    pub fn ckflt(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 3;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 1:2 - Clock Polarity"]
    #[inline]
    pub fn ckpol(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 1;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bit 0 - Clock selector"]
    #[inline]
    pub fn cksel(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bit 24 - Encoder mode enable"]
    #[inline]
    pub fn clear_enc(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 24);
        self
    }
    #[doc = "Bit 24 - Encoder mode enable"]
    #[inline]
    pub fn set_enc(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 24;
        self
    }
    #[doc = "Bit 24 - Encoder mode enable"]
    #[inline]
    pub fn enc(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 24;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 23 - counter mode enabled"]
    #[inline]
    pub fn clear_countmode(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 23);
        self
    }
    #[doc = "Bit 23 - counter mode enabled"]
    #[inline]
    pub fn set_countmode(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 23;
        self
    }
    #[doc = "Bit 23 - counter mode enabled"]
    #[inline]
    pub fn countmode(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 23;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 22 - Registers update mode"]
    #[inline]
    pub fn clear_preload(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 22);
        self
    }
    #[doc = "Bit 22 - Registers update mode"]
    #[inline]
    pub fn set_preload(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 22;
        self
    }
    #[doc = "Bit 22 - Registers update mode"]
    #[inline]
    pub fn preload(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 22;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 21 - Waveform shape polarity"]
    #[inline]
    pub fn clear_wavpol(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 21);
        self
    }
    #[doc = "Bit 21 - Waveform shape polarity"]
    #[inline]
    pub fn set_wavpol(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 21;
        self
    }
    #[doc = "Bit 21 - Waveform shape polarity"]
    #[inline]
    pub fn wavpol(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 21;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 20 - Waveform shape"]
    #[inline]
    pub fn clear_wave(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 20);
        self
    }
    #[doc = "Bit 20 - Waveform shape"]
    #[inline]
    pub fn set_wave(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 20;
        self
    }
    #[doc = "Bit 20 - Waveform shape"]
    #[inline]
    pub fn wave(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 20;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 19 - Timeout enable"]
    #[inline]
    pub fn clear_timout(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 19);
        self
    }
    #[doc = "Bit 19 - Timeout enable"]
    #[inline]
    pub fn set_timout(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 19;
        self
    }
    #[doc = "Bit 19 - Timeout enable"]
    #[inline]
    pub fn timout(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 19;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 17:18 - Trigger enable and polarity"]
    #[inline]
    pub unsafe fn trigen(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 17;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 13:15 - Trigger selector"]
    #[inline]
    pub unsafe fn trigsel(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 7;
        const OFFSET: u8 = 13;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 9:11 - Clock prescaler"]
    #[inline]
    pub unsafe fn presc(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 7;
        const OFFSET: u8 = 9;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 6:7 - Configurable digital filter for trigger"]
    #[inline]
    pub unsafe fn trgflt(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 6;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 3:4 - Configurable digital filter for external clock"]
    #[inline]
    pub unsafe fn ckflt(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 3;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 1:2 - Clock Polarity"]
    #[inline]
    pub unsafe fn ckpol(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 1;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 0 - Clock selector"]
    #[inline]
    pub fn clear_cksel(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 0);
        self
    }
    #[doc = "Bit 0 - Clock selector"]
    #[inline]
    pub fn set_cksel(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 0;
        self
    }
    #[doc = "Bit 0 - Clock selector"]
    #[inline]
    pub fn cksel(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
