#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::PR1 {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bit 0 - Pending bit 0"]
    #[inline]
    pub fn pr0(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 1 - Pending bit 1"]
    #[inline]
    pub fn pr1(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 2 - Pending bit 2"]
    #[inline]
    pub fn pr2(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 3 - Pending bit 3"]
    #[inline]
    pub fn pr3(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 3;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 4 - Pending bit 4"]
    #[inline]
    pub fn pr4(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 4;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 5 - Pending bit 5"]
    #[inline]
    pub fn pr5(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 5;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 6 - Pending bit 6"]
    #[inline]
    pub fn pr6(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 6;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 7 - Pending bit 7"]
    #[inline]
    pub fn pr7(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 7;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 8 - Pending bit 8"]
    #[inline]
    pub fn pr8(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 8;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 9 - Pending bit 9"]
    #[inline]
    pub fn pr9(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 9;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 10 - Pending bit 10"]
    #[inline]
    pub fn pr10(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 10;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 11 - Pending bit 11"]
    #[inline]
    pub fn pr11(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 11;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 12 - Pending bit 12"]
    #[inline]
    pub fn pr12(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 12;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 13 - Pending bit 13"]
    #[inline]
    pub fn pr13(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 13;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 14 - Pending bit 14"]
    #[inline]
    pub fn pr14(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 14;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 15 - Pending bit 15"]
    #[inline]
    pub fn pr15(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 15;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 16 - Pending bit 16"]
    #[inline]
    pub fn pr16(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 16;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 18 - Pending bit 18"]
    #[inline]
    pub fn pr18(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 18;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 19 - Pending bit 19"]
    #[inline]
    pub fn pr19(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 19;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 20 - Pending bit 20"]
    #[inline]
    pub fn pr20(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 20;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 21 - Pending bit 21"]
    #[inline]
    pub fn pr21(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 21;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 22 - Pending bit 22"]
    #[inline]
    pub fn pr22(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 22;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bit 0 - Pending bit 0"]
    #[inline]
    pub fn clear_pr0(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 0);
        self
    }
    #[doc = "Bit 0 - Pending bit 0"]
    #[inline]
    pub fn set_pr0(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 0;
        self
    }
    #[doc = "Bit 0 - Pending bit 0"]
    #[inline]
    pub fn pr0(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 1 - Pending bit 1"]
    #[inline]
    pub fn clear_pr1(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 1);
        self
    }
    #[doc = "Bit 1 - Pending bit 1"]
    #[inline]
    pub fn set_pr1(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 1;
        self
    }
    #[doc = "Bit 1 - Pending bit 1"]
    #[inline]
    pub fn pr1(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 2 - Pending bit 2"]
    #[inline]
    pub fn clear_pr2(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 2);
        self
    }
    #[doc = "Bit 2 - Pending bit 2"]
    #[inline]
    pub fn set_pr2(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 2;
        self
    }
    #[doc = "Bit 2 - Pending bit 2"]
    #[inline]
    pub fn pr2(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 3 - Pending bit 3"]
    #[inline]
    pub fn clear_pr3(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 3);
        self
    }
    #[doc = "Bit 3 - Pending bit 3"]
    #[inline]
    pub fn set_pr3(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 3;
        self
    }
    #[doc = "Bit 3 - Pending bit 3"]
    #[inline]
    pub fn pr3(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 3;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 4 - Pending bit 4"]
    #[inline]
    pub fn clear_pr4(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 4);
        self
    }
    #[doc = "Bit 4 - Pending bit 4"]
    #[inline]
    pub fn set_pr4(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 4;
        self
    }
    #[doc = "Bit 4 - Pending bit 4"]
    #[inline]
    pub fn pr4(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 4;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 5 - Pending bit 5"]
    #[inline]
    pub fn clear_pr5(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 5);
        self
    }
    #[doc = "Bit 5 - Pending bit 5"]
    #[inline]
    pub fn set_pr5(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 5;
        self
    }
    #[doc = "Bit 5 - Pending bit 5"]
    #[inline]
    pub fn pr5(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 5;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 6 - Pending bit 6"]
    #[inline]
    pub fn clear_pr6(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 6);
        self
    }
    #[doc = "Bit 6 - Pending bit 6"]
    #[inline]
    pub fn set_pr6(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 6;
        self
    }
    #[doc = "Bit 6 - Pending bit 6"]
    #[inline]
    pub fn pr6(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 6;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 7 - Pending bit 7"]
    #[inline]
    pub fn clear_pr7(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 7);
        self
    }
    #[doc = "Bit 7 - Pending bit 7"]
    #[inline]
    pub fn set_pr7(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 7;
        self
    }
    #[doc = "Bit 7 - Pending bit 7"]
    #[inline]
    pub fn pr7(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 7;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 8 - Pending bit 8"]
    #[inline]
    pub fn clear_pr8(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 8);
        self
    }
    #[doc = "Bit 8 - Pending bit 8"]
    #[inline]
    pub fn set_pr8(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 8;
        self
    }
    #[doc = "Bit 8 - Pending bit 8"]
    #[inline]
    pub fn pr8(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 8;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 9 - Pending bit 9"]
    #[inline]
    pub fn clear_pr9(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 9);
        self
    }
    #[doc = "Bit 9 - Pending bit 9"]
    #[inline]
    pub fn set_pr9(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 9;
        self
    }
    #[doc = "Bit 9 - Pending bit 9"]
    #[inline]
    pub fn pr9(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 9;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 10 - Pending bit 10"]
    #[inline]
    pub fn clear_pr10(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 10);
        self
    }
    #[doc = "Bit 10 - Pending bit 10"]
    #[inline]
    pub fn set_pr10(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 10;
        self
    }
    #[doc = "Bit 10 - Pending bit 10"]
    #[inline]
    pub fn pr10(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 10;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 11 - Pending bit 11"]
    #[inline]
    pub fn clear_pr11(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 11);
        self
    }
    #[doc = "Bit 11 - Pending bit 11"]
    #[inline]
    pub fn set_pr11(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 11;
        self
    }
    #[doc = "Bit 11 - Pending bit 11"]
    #[inline]
    pub fn pr11(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 11;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 12 - Pending bit 12"]
    #[inline]
    pub fn clear_pr12(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 12);
        self
    }
    #[doc = "Bit 12 - Pending bit 12"]
    #[inline]
    pub fn set_pr12(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 12;
        self
    }
    #[doc = "Bit 12 - Pending bit 12"]
    #[inline]
    pub fn pr12(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 12;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 13 - Pending bit 13"]
    #[inline]
    pub fn clear_pr13(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 13);
        self
    }
    #[doc = "Bit 13 - Pending bit 13"]
    #[inline]
    pub fn set_pr13(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 13;
        self
    }
    #[doc = "Bit 13 - Pending bit 13"]
    #[inline]
    pub fn pr13(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 13;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 14 - Pending bit 14"]
    #[inline]
    pub fn clear_pr14(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 14);
        self
    }
    #[doc = "Bit 14 - Pending bit 14"]
    #[inline]
    pub fn set_pr14(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 14;
        self
    }
    #[doc = "Bit 14 - Pending bit 14"]
    #[inline]
    pub fn pr14(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 14;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 15 - Pending bit 15"]
    #[inline]
    pub fn clear_pr15(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 15);
        self
    }
    #[doc = "Bit 15 - Pending bit 15"]
    #[inline]
    pub fn set_pr15(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 15;
        self
    }
    #[doc = "Bit 15 - Pending bit 15"]
    #[inline]
    pub fn pr15(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 15;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 16 - Pending bit 16"]
    #[inline]
    pub fn clear_pr16(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 16);
        self
    }
    #[doc = "Bit 16 - Pending bit 16"]
    #[inline]
    pub fn set_pr16(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 16;
        self
    }
    #[doc = "Bit 16 - Pending bit 16"]
    #[inline]
    pub fn pr16(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 16;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 18 - Pending bit 18"]
    #[inline]
    pub fn clear_pr18(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 18);
        self
    }
    #[doc = "Bit 18 - Pending bit 18"]
    #[inline]
    pub fn set_pr18(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 18;
        self
    }
    #[doc = "Bit 18 - Pending bit 18"]
    #[inline]
    pub fn pr18(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 18;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 19 - Pending bit 19"]
    #[inline]
    pub fn clear_pr19(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 19);
        self
    }
    #[doc = "Bit 19 - Pending bit 19"]
    #[inline]
    pub fn set_pr19(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 19;
        self
    }
    #[doc = "Bit 19 - Pending bit 19"]
    #[inline]
    pub fn pr19(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 19;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 20 - Pending bit 20"]
    #[inline]
    pub fn clear_pr20(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 20);
        self
    }
    #[doc = "Bit 20 - Pending bit 20"]
    #[inline]
    pub fn set_pr20(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 20;
        self
    }
    #[doc = "Bit 20 - Pending bit 20"]
    #[inline]
    pub fn pr20(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 20;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 21 - Pending bit 21"]
    #[inline]
    pub fn clear_pr21(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 21);
        self
    }
    #[doc = "Bit 21 - Pending bit 21"]
    #[inline]
    pub fn set_pr21(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 21;
        self
    }
    #[doc = "Bit 21 - Pending bit 21"]
    #[inline]
    pub fn pr21(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 21;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 22 - Pending bit 22"]
    #[inline]
    pub fn clear_pr22(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 22);
        self
    }
    #[doc = "Bit 22 - Pending bit 22"]
    #[inline]
    pub fn set_pr22(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 22;
        self
    }
    #[doc = "Bit 22 - Pending bit 22"]
    #[inline]
    pub fn pr22(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 22;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
