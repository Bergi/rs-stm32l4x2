#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
impl super::RI0R {
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bits 21:31 - STID"]
    #[inline]
    pub fn stid(&self) -> u16 {
        const MASK: u16 = 2047;
        const OFFSET: u8 = 21;
        ((self.bits >> OFFSET) & MASK as u32) as u16
    }
    #[doc = "Bits 3:20 - EXID"]
    #[inline]
    pub fn exid(&self) -> u32 {
        const MASK: u32 = 262143;
        const OFFSET: u8 = 3;
        ((self.bits >> OFFSET) & MASK as u32) as u32
    }
    #[doc = "Bit 2 - IDE"]
    #[inline]
    pub fn ide(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 1 - RTR"]
    #[inline]
    pub fn rtr(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
}
