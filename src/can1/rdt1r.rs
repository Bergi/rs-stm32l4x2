#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
impl super::RDT1R {
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bits 16:31 - TIME"]
    #[inline]
    pub fn time(&self) -> u16 {
        const MASK: u16 = 65535;
        const OFFSET: u8 = 16;
        ((self.bits >> OFFSET) & MASK as u32) as u16
    }
    #[doc = "Bits 8:15 - FMI"]
    #[inline]
    pub fn fmi(&self) -> u8 {
        const MASK: u8 = 255;
        const OFFSET: u8 = 8;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 0:3 - DLC"]
    #[inline]
    pub fn dlc(&self) -> u8 {
        const MASK: u8 = 15;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
}
