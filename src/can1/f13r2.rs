#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::F13R2 {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bit 0 - Filter bits"]
    #[inline]
    pub fn fb0(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 1 - Filter bits"]
    #[inline]
    pub fn fb1(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 2 - Filter bits"]
    #[inline]
    pub fn fb2(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 3 - Filter bits"]
    #[inline]
    pub fn fb3(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 3;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 4 - Filter bits"]
    #[inline]
    pub fn fb4(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 4;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 5 - Filter bits"]
    #[inline]
    pub fn fb5(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 5;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 6 - Filter bits"]
    #[inline]
    pub fn fb6(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 6;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 7 - Filter bits"]
    #[inline]
    pub fn fb7(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 7;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 8 - Filter bits"]
    #[inline]
    pub fn fb8(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 8;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 9 - Filter bits"]
    #[inline]
    pub fn fb9(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 9;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 10 - Filter bits"]
    #[inline]
    pub fn fb10(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 10;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 11 - Filter bits"]
    #[inline]
    pub fn fb11(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 11;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 12 - Filter bits"]
    #[inline]
    pub fn fb12(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 12;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 13 - Filter bits"]
    #[inline]
    pub fn fb13(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 13;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 14 - Filter bits"]
    #[inline]
    pub fn fb14(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 14;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 15 - Filter bits"]
    #[inline]
    pub fn fb15(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 15;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 16 - Filter bits"]
    #[inline]
    pub fn fb16(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 16;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 17 - Filter bits"]
    #[inline]
    pub fn fb17(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 17;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 18 - Filter bits"]
    #[inline]
    pub fn fb18(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 18;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 19 - Filter bits"]
    #[inline]
    pub fn fb19(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 19;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 20 - Filter bits"]
    #[inline]
    pub fn fb20(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 20;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 21 - Filter bits"]
    #[inline]
    pub fn fb21(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 21;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 22 - Filter bits"]
    #[inline]
    pub fn fb22(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 22;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 23 - Filter bits"]
    #[inline]
    pub fn fb23(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 23;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 24 - Filter bits"]
    #[inline]
    pub fn fb24(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 24;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 25 - Filter bits"]
    #[inline]
    pub fn fb25(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 25;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 26 - Filter bits"]
    #[inline]
    pub fn fb26(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 26;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 27 - Filter bits"]
    #[inline]
    pub fn fb27(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 27;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 28 - Filter bits"]
    #[inline]
    pub fn fb28(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 28;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 29 - Filter bits"]
    #[inline]
    pub fn fb29(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 29;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 30 - Filter bits"]
    #[inline]
    pub fn fb30(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 30;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 31 - Filter bits"]
    #[inline]
    pub fn fb31(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 31;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bit 0 - Filter bits"]
    #[inline]
    pub fn clear_fb0(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 0);
        self
    }
    #[doc = "Bit 0 - Filter bits"]
    #[inline]
    pub fn set_fb0(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 0;
        self
    }
    #[doc = "Bit 0 - Filter bits"]
    #[inline]
    pub fn fb0(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 1 - Filter bits"]
    #[inline]
    pub fn clear_fb1(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 1);
        self
    }
    #[doc = "Bit 1 - Filter bits"]
    #[inline]
    pub fn set_fb1(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 1;
        self
    }
    #[doc = "Bit 1 - Filter bits"]
    #[inline]
    pub fn fb1(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 2 - Filter bits"]
    #[inline]
    pub fn clear_fb2(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 2);
        self
    }
    #[doc = "Bit 2 - Filter bits"]
    #[inline]
    pub fn set_fb2(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 2;
        self
    }
    #[doc = "Bit 2 - Filter bits"]
    #[inline]
    pub fn fb2(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 3 - Filter bits"]
    #[inline]
    pub fn clear_fb3(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 3);
        self
    }
    #[doc = "Bit 3 - Filter bits"]
    #[inline]
    pub fn set_fb3(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 3;
        self
    }
    #[doc = "Bit 3 - Filter bits"]
    #[inline]
    pub fn fb3(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 3;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 4 - Filter bits"]
    #[inline]
    pub fn clear_fb4(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 4);
        self
    }
    #[doc = "Bit 4 - Filter bits"]
    #[inline]
    pub fn set_fb4(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 4;
        self
    }
    #[doc = "Bit 4 - Filter bits"]
    #[inline]
    pub fn fb4(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 4;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 5 - Filter bits"]
    #[inline]
    pub fn clear_fb5(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 5);
        self
    }
    #[doc = "Bit 5 - Filter bits"]
    #[inline]
    pub fn set_fb5(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 5;
        self
    }
    #[doc = "Bit 5 - Filter bits"]
    #[inline]
    pub fn fb5(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 5;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 6 - Filter bits"]
    #[inline]
    pub fn clear_fb6(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 6);
        self
    }
    #[doc = "Bit 6 - Filter bits"]
    #[inline]
    pub fn set_fb6(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 6;
        self
    }
    #[doc = "Bit 6 - Filter bits"]
    #[inline]
    pub fn fb6(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 6;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 7 - Filter bits"]
    #[inline]
    pub fn clear_fb7(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 7);
        self
    }
    #[doc = "Bit 7 - Filter bits"]
    #[inline]
    pub fn set_fb7(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 7;
        self
    }
    #[doc = "Bit 7 - Filter bits"]
    #[inline]
    pub fn fb7(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 7;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 8 - Filter bits"]
    #[inline]
    pub fn clear_fb8(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 8);
        self
    }
    #[doc = "Bit 8 - Filter bits"]
    #[inline]
    pub fn set_fb8(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 8;
        self
    }
    #[doc = "Bit 8 - Filter bits"]
    #[inline]
    pub fn fb8(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 8;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 9 - Filter bits"]
    #[inline]
    pub fn clear_fb9(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 9);
        self
    }
    #[doc = "Bit 9 - Filter bits"]
    #[inline]
    pub fn set_fb9(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 9;
        self
    }
    #[doc = "Bit 9 - Filter bits"]
    #[inline]
    pub fn fb9(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 9;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 10 - Filter bits"]
    #[inline]
    pub fn clear_fb10(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 10);
        self
    }
    #[doc = "Bit 10 - Filter bits"]
    #[inline]
    pub fn set_fb10(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 10;
        self
    }
    #[doc = "Bit 10 - Filter bits"]
    #[inline]
    pub fn fb10(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 10;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 11 - Filter bits"]
    #[inline]
    pub fn clear_fb11(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 11);
        self
    }
    #[doc = "Bit 11 - Filter bits"]
    #[inline]
    pub fn set_fb11(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 11;
        self
    }
    #[doc = "Bit 11 - Filter bits"]
    #[inline]
    pub fn fb11(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 11;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 12 - Filter bits"]
    #[inline]
    pub fn clear_fb12(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 12);
        self
    }
    #[doc = "Bit 12 - Filter bits"]
    #[inline]
    pub fn set_fb12(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 12;
        self
    }
    #[doc = "Bit 12 - Filter bits"]
    #[inline]
    pub fn fb12(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 12;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 13 - Filter bits"]
    #[inline]
    pub fn clear_fb13(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 13);
        self
    }
    #[doc = "Bit 13 - Filter bits"]
    #[inline]
    pub fn set_fb13(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 13;
        self
    }
    #[doc = "Bit 13 - Filter bits"]
    #[inline]
    pub fn fb13(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 13;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 14 - Filter bits"]
    #[inline]
    pub fn clear_fb14(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 14);
        self
    }
    #[doc = "Bit 14 - Filter bits"]
    #[inline]
    pub fn set_fb14(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 14;
        self
    }
    #[doc = "Bit 14 - Filter bits"]
    #[inline]
    pub fn fb14(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 14;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 15 - Filter bits"]
    #[inline]
    pub fn clear_fb15(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 15);
        self
    }
    #[doc = "Bit 15 - Filter bits"]
    #[inline]
    pub fn set_fb15(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 15;
        self
    }
    #[doc = "Bit 15 - Filter bits"]
    #[inline]
    pub fn fb15(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 15;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 16 - Filter bits"]
    #[inline]
    pub fn clear_fb16(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 16);
        self
    }
    #[doc = "Bit 16 - Filter bits"]
    #[inline]
    pub fn set_fb16(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 16;
        self
    }
    #[doc = "Bit 16 - Filter bits"]
    #[inline]
    pub fn fb16(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 16;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 17 - Filter bits"]
    #[inline]
    pub fn clear_fb17(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 17);
        self
    }
    #[doc = "Bit 17 - Filter bits"]
    #[inline]
    pub fn set_fb17(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 17;
        self
    }
    #[doc = "Bit 17 - Filter bits"]
    #[inline]
    pub fn fb17(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 17;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 18 - Filter bits"]
    #[inline]
    pub fn clear_fb18(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 18);
        self
    }
    #[doc = "Bit 18 - Filter bits"]
    #[inline]
    pub fn set_fb18(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 18;
        self
    }
    #[doc = "Bit 18 - Filter bits"]
    #[inline]
    pub fn fb18(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 18;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 19 - Filter bits"]
    #[inline]
    pub fn clear_fb19(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 19);
        self
    }
    #[doc = "Bit 19 - Filter bits"]
    #[inline]
    pub fn set_fb19(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 19;
        self
    }
    #[doc = "Bit 19 - Filter bits"]
    #[inline]
    pub fn fb19(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 19;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 20 - Filter bits"]
    #[inline]
    pub fn clear_fb20(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 20);
        self
    }
    #[doc = "Bit 20 - Filter bits"]
    #[inline]
    pub fn set_fb20(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 20;
        self
    }
    #[doc = "Bit 20 - Filter bits"]
    #[inline]
    pub fn fb20(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 20;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 21 - Filter bits"]
    #[inline]
    pub fn clear_fb21(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 21);
        self
    }
    #[doc = "Bit 21 - Filter bits"]
    #[inline]
    pub fn set_fb21(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 21;
        self
    }
    #[doc = "Bit 21 - Filter bits"]
    #[inline]
    pub fn fb21(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 21;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 22 - Filter bits"]
    #[inline]
    pub fn clear_fb22(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 22);
        self
    }
    #[doc = "Bit 22 - Filter bits"]
    #[inline]
    pub fn set_fb22(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 22;
        self
    }
    #[doc = "Bit 22 - Filter bits"]
    #[inline]
    pub fn fb22(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 22;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 23 - Filter bits"]
    #[inline]
    pub fn clear_fb23(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 23);
        self
    }
    #[doc = "Bit 23 - Filter bits"]
    #[inline]
    pub fn set_fb23(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 23;
        self
    }
    #[doc = "Bit 23 - Filter bits"]
    #[inline]
    pub fn fb23(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 23;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 24 - Filter bits"]
    #[inline]
    pub fn clear_fb24(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 24);
        self
    }
    #[doc = "Bit 24 - Filter bits"]
    #[inline]
    pub fn set_fb24(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 24;
        self
    }
    #[doc = "Bit 24 - Filter bits"]
    #[inline]
    pub fn fb24(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 24;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 25 - Filter bits"]
    #[inline]
    pub fn clear_fb25(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 25);
        self
    }
    #[doc = "Bit 25 - Filter bits"]
    #[inline]
    pub fn set_fb25(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 25;
        self
    }
    #[doc = "Bit 25 - Filter bits"]
    #[inline]
    pub fn fb25(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 25;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 26 - Filter bits"]
    #[inline]
    pub fn clear_fb26(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 26);
        self
    }
    #[doc = "Bit 26 - Filter bits"]
    #[inline]
    pub fn set_fb26(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 26;
        self
    }
    #[doc = "Bit 26 - Filter bits"]
    #[inline]
    pub fn fb26(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 26;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 27 - Filter bits"]
    #[inline]
    pub fn clear_fb27(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 27);
        self
    }
    #[doc = "Bit 27 - Filter bits"]
    #[inline]
    pub fn set_fb27(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 27;
        self
    }
    #[doc = "Bit 27 - Filter bits"]
    #[inline]
    pub fn fb27(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 27;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 28 - Filter bits"]
    #[inline]
    pub fn clear_fb28(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 28);
        self
    }
    #[doc = "Bit 28 - Filter bits"]
    #[inline]
    pub fn set_fb28(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 28;
        self
    }
    #[doc = "Bit 28 - Filter bits"]
    #[inline]
    pub fn fb28(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 28;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 29 - Filter bits"]
    #[inline]
    pub fn clear_fb29(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 29);
        self
    }
    #[doc = "Bit 29 - Filter bits"]
    #[inline]
    pub fn set_fb29(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 29;
        self
    }
    #[doc = "Bit 29 - Filter bits"]
    #[inline]
    pub fn fb29(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 29;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 30 - Filter bits"]
    #[inline]
    pub fn clear_fb30(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 30);
        self
    }
    #[doc = "Bit 30 - Filter bits"]
    #[inline]
    pub fn set_fb30(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 30;
        self
    }
    #[doc = "Bit 30 - Filter bits"]
    #[inline]
    pub fn fb30(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 30;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 31 - Filter bits"]
    #[inline]
    pub fn clear_fb31(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 31);
        self
    }
    #[doc = "Bit 31 - Filter bits"]
    #[inline]
    pub fn set_fb31(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 31;
        self
    }
    #[doc = "Bit 31 - Filter bits"]
    #[inline]
    pub fn fb31(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 31;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
