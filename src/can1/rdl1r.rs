#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
impl super::RDL1R {
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bits 24:31 - DATA3"]
    #[inline]
    pub fn data3(&self) -> u8 {
        const MASK: u8 = 255;
        const OFFSET: u8 = 24;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 16:23 - DATA2"]
    #[inline]
    pub fn data2(&self) -> u8 {
        const MASK: u8 = 255;
        const OFFSET: u8 = 16;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 8:15 - DATA1"]
    #[inline]
    pub fn data1(&self) -> u8 {
        const MASK: u8 = 255;
        const OFFSET: u8 = 8;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 0:7 - DATA0"]
    #[inline]
    pub fn data0(&self) -> u8 {
        const MASK: u8 = 255;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
}
