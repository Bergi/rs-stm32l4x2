#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::CCR {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bit 31 - Double data rate mode"]
    #[inline]
    pub fn ddrm(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 31;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 30 - DDR hold half cycle"]
    #[inline]
    pub fn dhhc(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 30;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 28 - Send instruction only once mode"]
    #[inline]
    pub fn sioo(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 28;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bits 26:27 - Functional mode"]
    #[inline]
    pub fn fmode(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 26;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 24:25 - Data mode"]
    #[inline]
    pub fn dmode(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 24;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 18:22 - Number of dummy cycles"]
    #[inline]
    pub fn dcyc(&self) -> u8 {
        const MASK: u8 = 31;
        const OFFSET: u8 = 18;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 16:17 - Alternate bytes size"]
    #[inline]
    pub fn absize(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 16;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 14:15 - Alternate bytes mode"]
    #[inline]
    pub fn abmode(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 14;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 12:13 - Address size"]
    #[inline]
    pub fn adsize(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 12;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 10:11 - Address mode"]
    #[inline]
    pub fn admode(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 10;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 8:9 - Instruction mode"]
    #[inline]
    pub fn imode(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 8;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 0:7 - Instruction"]
    #[inline]
    pub fn instruction(&self) -> u8 {
        const MASK: u8 = 255;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bit 31 - Double data rate mode"]
    #[inline]
    pub fn clear_ddrm(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 31);
        self
    }
    #[doc = "Bit 31 - Double data rate mode"]
    #[inline]
    pub fn set_ddrm(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 31;
        self
    }
    #[doc = "Bit 31 - Double data rate mode"]
    #[inline]
    pub fn ddrm(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 31;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 30 - DDR hold half cycle"]
    #[inline]
    pub fn clear_dhhc(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 30);
        self
    }
    #[doc = "Bit 30 - DDR hold half cycle"]
    #[inline]
    pub fn set_dhhc(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 30;
        self
    }
    #[doc = "Bit 30 - DDR hold half cycle"]
    #[inline]
    pub fn dhhc(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 30;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 28 - Send instruction only once mode"]
    #[inline]
    pub fn clear_sioo(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 28);
        self
    }
    #[doc = "Bit 28 - Send instruction only once mode"]
    #[inline]
    pub fn set_sioo(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 28;
        self
    }
    #[doc = "Bit 28 - Send instruction only once mode"]
    #[inline]
    pub fn sioo(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 28;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 26:27 - Functional mode"]
    #[inline]
    pub unsafe fn fmode(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 26;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 24:25 - Data mode"]
    #[inline]
    pub unsafe fn dmode(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 24;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 18:22 - Number of dummy cycles"]
    #[inline]
    pub unsafe fn dcyc(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 31;
        const OFFSET: u8 = 18;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 16:17 - Alternate bytes size"]
    #[inline]
    pub unsafe fn absize(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 16;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 14:15 - Alternate bytes mode"]
    #[inline]
    pub unsafe fn abmode(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 14;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 12:13 - Address size"]
    #[inline]
    pub unsafe fn adsize(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 12;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 10:11 - Address mode"]
    #[inline]
    pub unsafe fn admode(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 10;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 8:9 - Instruction mode"]
    #[inline]
    pub unsafe fn imode(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 8;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 0:7 - Instruction"]
    #[inline]
    pub unsafe fn instruction(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 255;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
