#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::COMP2_CSR {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bit 0 - Comparator 2 enable bit"]
    #[inline]
    pub fn comp2_en(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bits 2:3 - Power Mode of the comparator 2"]
    #[inline]
    pub fn comp2_pwrmode(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 2;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 4:6 - Comparator 2 Input Minus connection configuration bit"]
    #[inline]
    pub fn comp2_inmsel(&self) -> u8 {
        const MASK: u8 = 7;
        const OFFSET: u8 = 4;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 7:8 - Comparator 2 Input Plus connection configuration bit"]
    #[inline]
    pub fn comp2_inpsel(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 7;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bit 9 - Windows mode selection bit"]
    #[inline]
    pub fn comp2_winmode(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 9;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 15 - Comparator 2 polarity selection bit"]
    #[inline]
    pub fn comp2_polarity(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 15;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bits 16:17 - Comparator 2 hysteresis selection bits"]
    #[inline]
    pub fn comp2_hyst(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 16;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 18:20 - Comparator 2 blanking source selection bits"]
    #[inline]
    pub fn comp2_blanking(&self) -> u8 {
        const MASK: u8 = 7;
        const OFFSET: u8 = 18;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bit 22 - Scaler bridge enable"]
    #[inline]
    pub fn comp2_brgen(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 22;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 23 - Voltage scaler enable bit"]
    #[inline]
    pub fn comp2_scalen(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 23;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bits 25:26 - comparator 2 input minus extended selection bits"]
    #[inline]
    pub fn comp2_inmesel(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 25;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bit 30 - Comparator 2 output status bit"]
    #[inline]
    pub fn comp2_value(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 30;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bit 0 - Comparator 2 enable bit"]
    #[inline]
    pub fn clear_comp2_en(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 0);
        self
    }
    #[doc = "Bit 0 - Comparator 2 enable bit"]
    #[inline]
    pub fn set_comp2_en(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 0;
        self
    }
    #[doc = "Bit 0 - Comparator 2 enable bit"]
    #[inline]
    pub fn comp2_en(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 2:3 - Power Mode of the comparator 2"]
    #[inline]
    pub unsafe fn comp2_pwrmode(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 2;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 4:6 - Comparator 2 Input Minus connection configuration bit"]
    #[inline]
    pub unsafe fn comp2_inmsel(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 7;
        const OFFSET: u8 = 4;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 7:8 - Comparator 2 Input Plus connection configuration bit"]
    #[inline]
    pub unsafe fn comp2_inpsel(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 7;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 9 - Windows mode selection bit"]
    #[inline]
    pub fn clear_comp2_winmode(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 9);
        self
    }
    #[doc = "Bit 9 - Windows mode selection bit"]
    #[inline]
    pub fn set_comp2_winmode(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 9;
        self
    }
    #[doc = "Bit 9 - Windows mode selection bit"]
    #[inline]
    pub fn comp2_winmode(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 9;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 15 - Comparator 2 polarity selection bit"]
    #[inline]
    pub fn clear_comp2_polarity(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 15);
        self
    }
    #[doc = "Bit 15 - Comparator 2 polarity selection bit"]
    #[inline]
    pub fn set_comp2_polarity(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 15;
        self
    }
    #[doc = "Bit 15 - Comparator 2 polarity selection bit"]
    #[inline]
    pub fn comp2_polarity(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 15;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 16:17 - Comparator 2 hysteresis selection bits"]
    #[inline]
    pub unsafe fn comp2_hyst(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 16;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 18:20 - Comparator 2 blanking source selection bits"]
    #[inline]
    pub unsafe fn comp2_blanking(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 7;
        const OFFSET: u8 = 18;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 22 - Scaler bridge enable"]
    #[inline]
    pub fn clear_comp2_brgen(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 22);
        self
    }
    #[doc = "Bit 22 - Scaler bridge enable"]
    #[inline]
    pub fn set_comp2_brgen(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 22;
        self
    }
    #[doc = "Bit 22 - Scaler bridge enable"]
    #[inline]
    pub fn comp2_brgen(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 22;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 23 - Voltage scaler enable bit"]
    #[inline]
    pub fn clear_comp2_scalen(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 23);
        self
    }
    #[doc = "Bit 23 - Voltage scaler enable bit"]
    #[inline]
    pub fn set_comp2_scalen(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 23;
        self
    }
    #[doc = "Bit 23 - Voltage scaler enable bit"]
    #[inline]
    pub fn comp2_scalen(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 23;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 25:26 - comparator 2 input minus extended selection bits"]
    #[inline]
    pub unsafe fn comp2_inmesel(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 25;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 31 - COMP2_CSR register lock bit"]
    #[inline]
    pub fn clear_comp2_lock(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 31);
        self
    }
    #[doc = "Bit 31 - COMP2_CSR register lock bit"]
    #[inline]
    pub fn set_comp2_lock(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 31;
        self
    }
    #[doc = "Bit 31 - COMP2_CSR register lock bit"]
    #[inline]
    pub fn comp2_lock(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 31;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
