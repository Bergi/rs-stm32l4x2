#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
impl super::TXCRCR {
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bits 0:15 - Tx CRC register"]
    #[inline]
    pub fn tx_crc(&self) -> u16 {
        const MASK: u16 = 65535;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) as u16
    }
}
