#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::SMPR2 {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bits 24:26 - SMP18"]
    #[inline]
    pub fn smp18(&self) -> u8 {
        const MASK: u8 = 7;
        const OFFSET: u8 = 24;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 21:23 - SMP17"]
    #[inline]
    pub fn smp17(&self) -> u8 {
        const MASK: u8 = 7;
        const OFFSET: u8 = 21;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 18:20 - SMP16"]
    #[inline]
    pub fn smp16(&self) -> u8 {
        const MASK: u8 = 7;
        const OFFSET: u8 = 18;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 15:17 - SMP15"]
    #[inline]
    pub fn smp15(&self) -> u8 {
        const MASK: u8 = 7;
        const OFFSET: u8 = 15;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 12:14 - SMP14"]
    #[inline]
    pub fn smp14(&self) -> u8 {
        const MASK: u8 = 7;
        const OFFSET: u8 = 12;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 9:11 - SMP13"]
    #[inline]
    pub fn smp13(&self) -> u8 {
        const MASK: u8 = 7;
        const OFFSET: u8 = 9;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 6:8 - SMP12"]
    #[inline]
    pub fn smp12(&self) -> u8 {
        const MASK: u8 = 7;
        const OFFSET: u8 = 6;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 3:5 - SMP11"]
    #[inline]
    pub fn smp11(&self) -> u8 {
        const MASK: u8 = 7;
        const OFFSET: u8 = 3;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 0:2 - SMP10"]
    #[inline]
    pub fn smp10(&self) -> u8 {
        const MASK: u8 = 7;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bits 24:26 - SMP18"]
    #[inline]
    pub unsafe fn smp18(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 7;
        const OFFSET: u8 = 24;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 21:23 - SMP17"]
    #[inline]
    pub unsafe fn smp17(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 7;
        const OFFSET: u8 = 21;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 18:20 - SMP16"]
    #[inline]
    pub unsafe fn smp16(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 7;
        const OFFSET: u8 = 18;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 15:17 - SMP15"]
    #[inline]
    pub unsafe fn smp15(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 7;
        const OFFSET: u8 = 15;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 12:14 - SMP14"]
    #[inline]
    pub unsafe fn smp14(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 7;
        const OFFSET: u8 = 12;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 9:11 - SMP13"]
    #[inline]
    pub unsafe fn smp13(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 7;
        const OFFSET: u8 = 9;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 6:8 - SMP12"]
    #[inline]
    pub unsafe fn smp12(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 7;
        const OFFSET: u8 = 6;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 3:5 - SMP11"]
    #[inline]
    pub unsafe fn smp11(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 7;
        const OFFSET: u8 = 3;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 0:2 - SMP10"]
    #[inline]
    pub unsafe fn smp10(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 7;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
