#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::OFR1 {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bit 31 - OFFSET1_EN"]
    #[inline]
    pub fn offset1_en(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 31;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bits 26:30 - OFFSET1_CH"]
    #[inline]
    pub fn offset1_ch(&self) -> u8 {
        const MASK: u8 = 31;
        const OFFSET: u8 = 26;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 0:11 - OFFSET1"]
    #[inline]
    pub fn offset1(&self) -> u16 {
        const MASK: u16 = 4095;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) as u16
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bit 31 - OFFSET1_EN"]
    #[inline]
    pub fn clear_offset1_en(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 31);
        self
    }
    #[doc = "Bit 31 - OFFSET1_EN"]
    #[inline]
    pub fn set_offset1_en(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 31;
        self
    }
    #[doc = "Bit 31 - OFFSET1_EN"]
    #[inline]
    pub fn offset1_en(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 31;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 26:30 - OFFSET1_CH"]
    #[inline]
    pub unsafe fn offset1_ch(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 31;
        const OFFSET: u8 = 26;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 0:11 - OFFSET1"]
    #[inline]
    pub unsafe fn offset1(&mut self, value: u16) -> &mut Self {
        const MASK: u16 = 4095;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
