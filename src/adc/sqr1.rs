#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::SQR1 {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bits 24:28 - SQ4"]
    #[inline]
    pub fn sq4(&self) -> u8 {
        const MASK: u8 = 31;
        const OFFSET: u8 = 24;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 18:22 - SQ3"]
    #[inline]
    pub fn sq3(&self) -> u8 {
        const MASK: u8 = 31;
        const OFFSET: u8 = 18;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 12:16 - SQ2"]
    #[inline]
    pub fn sq2(&self) -> u8 {
        const MASK: u8 = 31;
        const OFFSET: u8 = 12;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 6:10 - SQ1"]
    #[inline]
    pub fn sq1(&self) -> u8 {
        const MASK: u8 = 31;
        const OFFSET: u8 = 6;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 0:3 - L3"]
    #[inline]
    pub fn l3(&self) -> u8 {
        const MASK: u8 = 15;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bits 24:28 - SQ4"]
    #[inline]
    pub unsafe fn sq4(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 31;
        const OFFSET: u8 = 24;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 18:22 - SQ3"]
    #[inline]
    pub unsafe fn sq3(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 31;
        const OFFSET: u8 = 18;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 12:16 - SQ2"]
    #[inline]
    pub unsafe fn sq2(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 31;
        const OFFSET: u8 = 12;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 6:10 - SQ1"]
    #[inline]
    pub unsafe fn sq1(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 31;
        const OFFSET: u8 = 6;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 0:3 - L3"]
    #[inline]
    pub unsafe fn l3(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 15;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
