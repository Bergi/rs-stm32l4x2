#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::CR1 {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bit 28 - Word length"]
    #[inline]
    pub fn m1(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 28;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 25 - Driver Enable assertion time"]
    #[inline]
    pub fn deat4(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 25;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 24 - DEAT3"]
    #[inline]
    pub fn deat3(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 24;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 23 - DEAT2"]
    #[inline]
    pub fn deat2(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 23;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 22 - DEAT1"]
    #[inline]
    pub fn deat1(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 22;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 21 - DEAT0"]
    #[inline]
    pub fn deat0(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 21;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 20 - Driver Enable de-assertion time"]
    #[inline]
    pub fn dedt4(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 20;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 19 - DEDT3"]
    #[inline]
    pub fn dedt3(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 19;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 18 - DEDT2"]
    #[inline]
    pub fn dedt2(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 18;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 17 - DEDT1"]
    #[inline]
    pub fn dedt1(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 17;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 16 - DEDT0"]
    #[inline]
    pub fn dedt0(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 16;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 14 - Character match interrupt enable"]
    #[inline]
    pub fn cmie(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 14;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 13 - Mute mode enable"]
    #[inline]
    pub fn mme(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 13;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 12 - Word length"]
    #[inline]
    pub fn m0(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 12;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 11 - Receiver wakeup method"]
    #[inline]
    pub fn wake(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 11;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 10 - Parity control enable"]
    #[inline]
    pub fn pce(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 10;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 9 - Parity selection"]
    #[inline]
    pub fn ps(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 9;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 8 - PE interrupt enable"]
    #[inline]
    pub fn peie(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 8;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 7 - interrupt enable"]
    #[inline]
    pub fn txeie(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 7;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 6 - Transmission complete interrupt enable"]
    #[inline]
    pub fn tcie(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 6;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 5 - RXNE interrupt enable"]
    #[inline]
    pub fn rxneie(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 5;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 4 - IDLE interrupt enable"]
    #[inline]
    pub fn idleie(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 4;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 3 - Transmitter enable"]
    #[inline]
    pub fn te(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 3;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 2 - Receiver enable"]
    #[inline]
    pub fn re(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 1 - USART enable in Stop mode"]
    #[inline]
    pub fn uesm(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 0 - USART enable"]
    #[inline]
    pub fn ue(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bit 28 - Word length"]
    #[inline]
    pub fn clear_m1(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 28);
        self
    }
    #[doc = "Bit 28 - Word length"]
    #[inline]
    pub fn set_m1(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 28;
        self
    }
    #[doc = "Bit 28 - Word length"]
    #[inline]
    pub fn m1(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 28;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 25 - Driver Enable assertion time"]
    #[inline]
    pub fn clear_deat4(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 25);
        self
    }
    #[doc = "Bit 25 - Driver Enable assertion time"]
    #[inline]
    pub fn set_deat4(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 25;
        self
    }
    #[doc = "Bit 25 - Driver Enable assertion time"]
    #[inline]
    pub fn deat4(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 25;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 24 - DEAT3"]
    #[inline]
    pub fn clear_deat3(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 24);
        self
    }
    #[doc = "Bit 24 - DEAT3"]
    #[inline]
    pub fn set_deat3(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 24;
        self
    }
    #[doc = "Bit 24 - DEAT3"]
    #[inline]
    pub fn deat3(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 24;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 23 - DEAT2"]
    #[inline]
    pub fn clear_deat2(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 23);
        self
    }
    #[doc = "Bit 23 - DEAT2"]
    #[inline]
    pub fn set_deat2(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 23;
        self
    }
    #[doc = "Bit 23 - DEAT2"]
    #[inline]
    pub fn deat2(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 23;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 22 - DEAT1"]
    #[inline]
    pub fn clear_deat1(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 22);
        self
    }
    #[doc = "Bit 22 - DEAT1"]
    #[inline]
    pub fn set_deat1(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 22;
        self
    }
    #[doc = "Bit 22 - DEAT1"]
    #[inline]
    pub fn deat1(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 22;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 21 - DEAT0"]
    #[inline]
    pub fn clear_deat0(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 21);
        self
    }
    #[doc = "Bit 21 - DEAT0"]
    #[inline]
    pub fn set_deat0(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 21;
        self
    }
    #[doc = "Bit 21 - DEAT0"]
    #[inline]
    pub fn deat0(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 21;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 20 - Driver Enable de-assertion time"]
    #[inline]
    pub fn clear_dedt4(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 20);
        self
    }
    #[doc = "Bit 20 - Driver Enable de-assertion time"]
    #[inline]
    pub fn set_dedt4(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 20;
        self
    }
    #[doc = "Bit 20 - Driver Enable de-assertion time"]
    #[inline]
    pub fn dedt4(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 20;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 19 - DEDT3"]
    #[inline]
    pub fn clear_dedt3(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 19);
        self
    }
    #[doc = "Bit 19 - DEDT3"]
    #[inline]
    pub fn set_dedt3(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 19;
        self
    }
    #[doc = "Bit 19 - DEDT3"]
    #[inline]
    pub fn dedt3(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 19;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 18 - DEDT2"]
    #[inline]
    pub fn clear_dedt2(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 18);
        self
    }
    #[doc = "Bit 18 - DEDT2"]
    #[inline]
    pub fn set_dedt2(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 18;
        self
    }
    #[doc = "Bit 18 - DEDT2"]
    #[inline]
    pub fn dedt2(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 18;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 17 - DEDT1"]
    #[inline]
    pub fn clear_dedt1(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 17);
        self
    }
    #[doc = "Bit 17 - DEDT1"]
    #[inline]
    pub fn set_dedt1(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 17;
        self
    }
    #[doc = "Bit 17 - DEDT1"]
    #[inline]
    pub fn dedt1(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 17;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 16 - DEDT0"]
    #[inline]
    pub fn clear_dedt0(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 16);
        self
    }
    #[doc = "Bit 16 - DEDT0"]
    #[inline]
    pub fn set_dedt0(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 16;
        self
    }
    #[doc = "Bit 16 - DEDT0"]
    #[inline]
    pub fn dedt0(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 16;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 14 - Character match interrupt enable"]
    #[inline]
    pub fn clear_cmie(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 14);
        self
    }
    #[doc = "Bit 14 - Character match interrupt enable"]
    #[inline]
    pub fn set_cmie(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 14;
        self
    }
    #[doc = "Bit 14 - Character match interrupt enable"]
    #[inline]
    pub fn cmie(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 14;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 13 - Mute mode enable"]
    #[inline]
    pub fn clear_mme(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 13);
        self
    }
    #[doc = "Bit 13 - Mute mode enable"]
    #[inline]
    pub fn set_mme(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 13;
        self
    }
    #[doc = "Bit 13 - Mute mode enable"]
    #[inline]
    pub fn mme(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 13;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 12 - Word length"]
    #[inline]
    pub fn clear_m0(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 12);
        self
    }
    #[doc = "Bit 12 - Word length"]
    #[inline]
    pub fn set_m0(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 12;
        self
    }
    #[doc = "Bit 12 - Word length"]
    #[inline]
    pub fn m0(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 12;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 11 - Receiver wakeup method"]
    #[inline]
    pub fn clear_wake(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 11);
        self
    }
    #[doc = "Bit 11 - Receiver wakeup method"]
    #[inline]
    pub fn set_wake(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 11;
        self
    }
    #[doc = "Bit 11 - Receiver wakeup method"]
    #[inline]
    pub fn wake(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 11;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 10 - Parity control enable"]
    #[inline]
    pub fn clear_pce(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 10);
        self
    }
    #[doc = "Bit 10 - Parity control enable"]
    #[inline]
    pub fn set_pce(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 10;
        self
    }
    #[doc = "Bit 10 - Parity control enable"]
    #[inline]
    pub fn pce(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 10;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 9 - Parity selection"]
    #[inline]
    pub fn clear_ps(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 9);
        self
    }
    #[doc = "Bit 9 - Parity selection"]
    #[inline]
    pub fn set_ps(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 9;
        self
    }
    #[doc = "Bit 9 - Parity selection"]
    #[inline]
    pub fn ps(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 9;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 8 - PE interrupt enable"]
    #[inline]
    pub fn clear_peie(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 8);
        self
    }
    #[doc = "Bit 8 - PE interrupt enable"]
    #[inline]
    pub fn set_peie(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 8;
        self
    }
    #[doc = "Bit 8 - PE interrupt enable"]
    #[inline]
    pub fn peie(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 8;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 7 - interrupt enable"]
    #[inline]
    pub fn clear_txeie(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 7);
        self
    }
    #[doc = "Bit 7 - interrupt enable"]
    #[inline]
    pub fn set_txeie(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 7;
        self
    }
    #[doc = "Bit 7 - interrupt enable"]
    #[inline]
    pub fn txeie(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 7;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 6 - Transmission complete interrupt enable"]
    #[inline]
    pub fn clear_tcie(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 6);
        self
    }
    #[doc = "Bit 6 - Transmission complete interrupt enable"]
    #[inline]
    pub fn set_tcie(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 6;
        self
    }
    #[doc = "Bit 6 - Transmission complete interrupt enable"]
    #[inline]
    pub fn tcie(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 6;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 5 - RXNE interrupt enable"]
    #[inline]
    pub fn clear_rxneie(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 5);
        self
    }
    #[doc = "Bit 5 - RXNE interrupt enable"]
    #[inline]
    pub fn set_rxneie(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 5;
        self
    }
    #[doc = "Bit 5 - RXNE interrupt enable"]
    #[inline]
    pub fn rxneie(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 5;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 4 - IDLE interrupt enable"]
    #[inline]
    pub fn clear_idleie(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 4);
        self
    }
    #[doc = "Bit 4 - IDLE interrupt enable"]
    #[inline]
    pub fn set_idleie(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 4;
        self
    }
    #[doc = "Bit 4 - IDLE interrupt enable"]
    #[inline]
    pub fn idleie(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 4;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 3 - Transmitter enable"]
    #[inline]
    pub fn clear_te(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 3);
        self
    }
    #[doc = "Bit 3 - Transmitter enable"]
    #[inline]
    pub fn set_te(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 3;
        self
    }
    #[doc = "Bit 3 - Transmitter enable"]
    #[inline]
    pub fn te(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 3;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 2 - Receiver enable"]
    #[inline]
    pub fn clear_re(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 2);
        self
    }
    #[doc = "Bit 2 - Receiver enable"]
    #[inline]
    pub fn set_re(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 2;
        self
    }
    #[doc = "Bit 2 - Receiver enable"]
    #[inline]
    pub fn re(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 1 - USART enable in Stop mode"]
    #[inline]
    pub fn clear_uesm(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 1);
        self
    }
    #[doc = "Bit 1 - USART enable in Stop mode"]
    #[inline]
    pub fn set_uesm(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 1;
        self
    }
    #[doc = "Bit 1 - USART enable in Stop mode"]
    #[inline]
    pub fn uesm(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 0 - USART enable"]
    #[inline]
    pub fn clear_ue(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 0);
        self
    }
    #[doc = "Bit 0 - USART enable"]
    #[inline]
    pub fn set_ue(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 0;
        self
    }
    #[doc = "Bit 0 - USART enable"]
    #[inline]
    pub fn ue(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
