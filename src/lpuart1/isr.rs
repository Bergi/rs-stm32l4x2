#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
impl super::ISR {
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bit 22 - REACK"]
    #[inline]
    pub fn reack(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 22;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 21 - TEACK"]
    #[inline]
    pub fn teack(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 21;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 20 - WUF"]
    #[inline]
    pub fn wuf(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 20;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 19 - RWU"]
    #[inline]
    pub fn rwu(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 19;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 18 - SBKF"]
    #[inline]
    pub fn sbkf(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 18;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 17 - CMF"]
    #[inline]
    pub fn cmf(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 17;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 16 - BUSY"]
    #[inline]
    pub fn busy(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 16;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 10 - CTS"]
    #[inline]
    pub fn cts(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 10;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 9 - CTSIF"]
    #[inline]
    pub fn ctsif(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 9;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 7 - TXE"]
    #[inline]
    pub fn txe(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 7;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 6 - TC"]
    #[inline]
    pub fn tc(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 6;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 5 - RXNE"]
    #[inline]
    pub fn rxne(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 5;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 4 - IDLE"]
    #[inline]
    pub fn idle(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 4;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 3 - ORE"]
    #[inline]
    pub fn ore(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 3;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 2 - NF"]
    #[inline]
    pub fn nf(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 1 - FE"]
    #[inline]
    pub fn fe(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 0 - PE"]
    #[inline]
    pub fn pe(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
}
