#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::CR2 {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bits 28:31 - Address of the USART node"]
    #[inline]
    pub fn add4_7(&self) -> u8 {
        const MASK: u8 = 15;
        const OFFSET: u8 = 28;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 24:27 - Address of the USART node"]
    #[inline]
    pub fn add0_3(&self) -> u8 {
        const MASK: u8 = 15;
        const OFFSET: u8 = 24;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bit 23 - Receiver timeout enable"]
    #[inline]
    pub fn rtoen(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 23;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 22 - Auto baud rate mode"]
    #[inline]
    pub fn abrmod1(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 22;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 21 - ABRMOD0"]
    #[inline]
    pub fn abrmod0(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 21;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 20 - Auto baud rate enable"]
    #[inline]
    pub fn abren(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 20;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 19 - Most significant bit first"]
    #[inline]
    pub fn msbfirst(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 19;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 18 - Binary data inversion"]
    #[inline]
    pub fn tainv(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 18;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 17 - TX pin active level inversion"]
    #[inline]
    pub fn txinv(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 17;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 16 - RX pin active level inversion"]
    #[inline]
    pub fn rxinv(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 16;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 15 - Swap TX/RX pins"]
    #[inline]
    pub fn swap(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 15;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 14 - LIN mode enable"]
    #[inline]
    pub fn linen(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 14;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bits 12:13 - STOP bits"]
    #[inline]
    pub fn stop(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 12;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bit 11 - Clock enable"]
    #[inline]
    pub fn clken(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 11;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 10 - Clock polarity"]
    #[inline]
    pub fn cpol(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 10;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 9 - Clock phase"]
    #[inline]
    pub fn cpha(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 9;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 8 - Last bit clock pulse"]
    #[inline]
    pub fn lbcl(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 8;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 6 - LIN break detection interrupt enable"]
    #[inline]
    pub fn lbdie(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 6;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 5 - LIN break detection length"]
    #[inline]
    pub fn lbdl(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 5;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 4 - 7-bit Address Detection/4-bit Address Detection"]
    #[inline]
    pub fn addm7(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 4;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bits 28:31 - Address of the USART node"]
    #[inline]
    pub unsafe fn add4_7(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 15;
        const OFFSET: u8 = 28;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 24:27 - Address of the USART node"]
    #[inline]
    pub unsafe fn add0_3(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 15;
        const OFFSET: u8 = 24;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 23 - Receiver timeout enable"]
    #[inline]
    pub fn clear_rtoen(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 23);
        self
    }
    #[doc = "Bit 23 - Receiver timeout enable"]
    #[inline]
    pub fn set_rtoen(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 23;
        self
    }
    #[doc = "Bit 23 - Receiver timeout enable"]
    #[inline]
    pub fn rtoen(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 23;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 22 - Auto baud rate mode"]
    #[inline]
    pub fn clear_abrmod1(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 22);
        self
    }
    #[doc = "Bit 22 - Auto baud rate mode"]
    #[inline]
    pub fn set_abrmod1(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 22;
        self
    }
    #[doc = "Bit 22 - Auto baud rate mode"]
    #[inline]
    pub fn abrmod1(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 22;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 21 - ABRMOD0"]
    #[inline]
    pub fn clear_abrmod0(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 21);
        self
    }
    #[doc = "Bit 21 - ABRMOD0"]
    #[inline]
    pub fn set_abrmod0(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 21;
        self
    }
    #[doc = "Bit 21 - ABRMOD0"]
    #[inline]
    pub fn abrmod0(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 21;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 20 - Auto baud rate enable"]
    #[inline]
    pub fn clear_abren(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 20);
        self
    }
    #[doc = "Bit 20 - Auto baud rate enable"]
    #[inline]
    pub fn set_abren(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 20;
        self
    }
    #[doc = "Bit 20 - Auto baud rate enable"]
    #[inline]
    pub fn abren(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 20;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 19 - Most significant bit first"]
    #[inline]
    pub fn clear_msbfirst(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 19);
        self
    }
    #[doc = "Bit 19 - Most significant bit first"]
    #[inline]
    pub fn set_msbfirst(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 19;
        self
    }
    #[doc = "Bit 19 - Most significant bit first"]
    #[inline]
    pub fn msbfirst(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 19;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 18 - Binary data inversion"]
    #[inline]
    pub fn clear_tainv(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 18);
        self
    }
    #[doc = "Bit 18 - Binary data inversion"]
    #[inline]
    pub fn set_tainv(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 18;
        self
    }
    #[doc = "Bit 18 - Binary data inversion"]
    #[inline]
    pub fn tainv(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 18;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 17 - TX pin active level inversion"]
    #[inline]
    pub fn clear_txinv(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 17);
        self
    }
    #[doc = "Bit 17 - TX pin active level inversion"]
    #[inline]
    pub fn set_txinv(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 17;
        self
    }
    #[doc = "Bit 17 - TX pin active level inversion"]
    #[inline]
    pub fn txinv(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 17;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 16 - RX pin active level inversion"]
    #[inline]
    pub fn clear_rxinv(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 16);
        self
    }
    #[doc = "Bit 16 - RX pin active level inversion"]
    #[inline]
    pub fn set_rxinv(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 16;
        self
    }
    #[doc = "Bit 16 - RX pin active level inversion"]
    #[inline]
    pub fn rxinv(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 16;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 15 - Swap TX/RX pins"]
    #[inline]
    pub fn clear_swap(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 15);
        self
    }
    #[doc = "Bit 15 - Swap TX/RX pins"]
    #[inline]
    pub fn set_swap(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 15;
        self
    }
    #[doc = "Bit 15 - Swap TX/RX pins"]
    #[inline]
    pub fn swap(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 15;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 14 - LIN mode enable"]
    #[inline]
    pub fn clear_linen(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 14);
        self
    }
    #[doc = "Bit 14 - LIN mode enable"]
    #[inline]
    pub fn set_linen(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 14;
        self
    }
    #[doc = "Bit 14 - LIN mode enable"]
    #[inline]
    pub fn linen(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 14;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 12:13 - STOP bits"]
    #[inline]
    pub unsafe fn stop(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 12;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 11 - Clock enable"]
    #[inline]
    pub fn clear_clken(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 11);
        self
    }
    #[doc = "Bit 11 - Clock enable"]
    #[inline]
    pub fn set_clken(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 11;
        self
    }
    #[doc = "Bit 11 - Clock enable"]
    #[inline]
    pub fn clken(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 11;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 10 - Clock polarity"]
    #[inline]
    pub fn clear_cpol(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 10);
        self
    }
    #[doc = "Bit 10 - Clock polarity"]
    #[inline]
    pub fn set_cpol(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 10;
        self
    }
    #[doc = "Bit 10 - Clock polarity"]
    #[inline]
    pub fn cpol(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 10;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 9 - Clock phase"]
    #[inline]
    pub fn clear_cpha(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 9);
        self
    }
    #[doc = "Bit 9 - Clock phase"]
    #[inline]
    pub fn set_cpha(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 9;
        self
    }
    #[doc = "Bit 9 - Clock phase"]
    #[inline]
    pub fn cpha(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 9;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 8 - Last bit clock pulse"]
    #[inline]
    pub fn clear_lbcl(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 8);
        self
    }
    #[doc = "Bit 8 - Last bit clock pulse"]
    #[inline]
    pub fn set_lbcl(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 8;
        self
    }
    #[doc = "Bit 8 - Last bit clock pulse"]
    #[inline]
    pub fn lbcl(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 8;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 6 - LIN break detection interrupt enable"]
    #[inline]
    pub fn clear_lbdie(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 6);
        self
    }
    #[doc = "Bit 6 - LIN break detection interrupt enable"]
    #[inline]
    pub fn set_lbdie(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 6;
        self
    }
    #[doc = "Bit 6 - LIN break detection interrupt enable"]
    #[inline]
    pub fn lbdie(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 6;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 5 - LIN break detection length"]
    #[inline]
    pub fn clear_lbdl(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 5);
        self
    }
    #[doc = "Bit 5 - LIN break detection length"]
    #[inline]
    pub fn set_lbdl(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 5;
        self
    }
    #[doc = "Bit 5 - LIN break detection length"]
    #[inline]
    pub fn lbdl(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 5;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 4 - 7-bit Address Detection/4-bit Address Detection"]
    #[inline]
    pub fn clear_addm7(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 4);
        self
    }
    #[doc = "Bit 4 - 7-bit Address Detection/4-bit Address Detection"]
    #[inline]
    pub fn set_addm7(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 4;
        self
    }
    #[doc = "Bit 4 - 7-bit Address Detection/4-bit Address Detection"]
    #[inline]
    pub fn addm7(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 4;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
