#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
impl super::PECR {
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bits 0:7 - Packet error checking register"]
    #[inline]
    pub fn pec(&self) -> u8 {
        const MASK: u8 = 255;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
}
