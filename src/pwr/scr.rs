#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::SCR {
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bit 8 - Clear standby flag"]
    #[inline]
    pub fn clear_sbf(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 8);
        self
    }
    #[doc = "Bit 8 - Clear standby flag"]
    #[inline]
    pub fn set_sbf(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 8;
        self
    }
    #[doc = "Bit 8 - Clear standby flag"]
    #[inline]
    pub fn sbf(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 8;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 4 - Clear wakeup flag 5"]
    #[inline]
    pub fn clear_wuf5(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 4);
        self
    }
    #[doc = "Bit 4 - Clear wakeup flag 5"]
    #[inline]
    pub fn set_wuf5(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 4;
        self
    }
    #[doc = "Bit 4 - Clear wakeup flag 5"]
    #[inline]
    pub fn wuf5(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 4;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 3 - Clear wakeup flag 4"]
    #[inline]
    pub fn clear_wuf4(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 3);
        self
    }
    #[doc = "Bit 3 - Clear wakeup flag 4"]
    #[inline]
    pub fn set_wuf4(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 3;
        self
    }
    #[doc = "Bit 3 - Clear wakeup flag 4"]
    #[inline]
    pub fn wuf4(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 3;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 2 - Clear wakeup flag 3"]
    #[inline]
    pub fn clear_wuf3(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 2);
        self
    }
    #[doc = "Bit 2 - Clear wakeup flag 3"]
    #[inline]
    pub fn set_wuf3(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 2;
        self
    }
    #[doc = "Bit 2 - Clear wakeup flag 3"]
    #[inline]
    pub fn wuf3(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 1 - Clear wakeup flag 2"]
    #[inline]
    pub fn clear_wuf2(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 1);
        self
    }
    #[doc = "Bit 1 - Clear wakeup flag 2"]
    #[inline]
    pub fn set_wuf2(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 1;
        self
    }
    #[doc = "Bit 1 - Clear wakeup flag 2"]
    #[inline]
    pub fn wuf2(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 0 - Clear wakeup flag 1"]
    #[inline]
    pub fn clear_wuf1(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 0);
        self
    }
    #[doc = "Bit 0 - Clear wakeup flag 1"]
    #[inline]
    pub fn set_wuf1(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 0;
        self
    }
    #[doc = "Bit 0 - Clear wakeup flag 1"]
    #[inline]
    pub fn wuf1(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
