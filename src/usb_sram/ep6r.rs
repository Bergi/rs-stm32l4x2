#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::EP6R {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bits 0:3 - Endpoint address"]
    #[inline]
    pub fn ea(&self) -> u8 {
        const MASK: u8 = 15;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bits 4:5 - Status bits, for transmission transfers"]
    #[inline]
    pub fn stat_tx(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 4;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bit 6 - Data Toggle, for transmission transfers"]
    #[inline]
    pub fn dtog_tx(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 6;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 7 - Correct Transfer for transmission"]
    #[inline]
    pub fn ctr_tx(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 7;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 8 - Endpoint kind"]
    #[inline]
    pub fn ep_kind(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 8;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bits 9:10 - Endpoint type"]
    #[inline]
    pub fn ep_type(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 9;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bit 11 - Setup transaction completed"]
    #[inline]
    pub fn setup(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 11;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bits 12:13 - Status bits, for reception transfers"]
    #[inline]
    pub fn stat_rx(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 12;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bit 14 - Data Toggle, for reception transfers"]
    #[inline]
    pub fn dtog_rx(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 14;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 15 - Correct transfer for reception"]
    #[inline]
    pub fn ctr_rx(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 15;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bits 0:3 - Endpoint address"]
    #[inline]
    pub unsafe fn ea(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 15;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 4:5 - Status bits, for transmission transfers"]
    #[inline]
    pub unsafe fn stat_tx(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 4;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 6 - Data Toggle, for transmission transfers"]
    #[inline]
    pub fn clear_dtog_tx(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 6);
        self
    }
    #[doc = "Bit 6 - Data Toggle, for transmission transfers"]
    #[inline]
    pub fn set_dtog_tx(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 6;
        self
    }
    #[doc = "Bit 6 - Data Toggle, for transmission transfers"]
    #[inline]
    pub fn dtog_tx(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 6;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 7 - Correct Transfer for transmission"]
    #[inline]
    pub fn clear_ctr_tx(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 7);
        self
    }
    #[doc = "Bit 7 - Correct Transfer for transmission"]
    #[inline]
    pub fn set_ctr_tx(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 7;
        self
    }
    #[doc = "Bit 7 - Correct Transfer for transmission"]
    #[inline]
    pub fn ctr_tx(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 7;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 8 - Endpoint kind"]
    #[inline]
    pub fn clear_ep_kind(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 8);
        self
    }
    #[doc = "Bit 8 - Endpoint kind"]
    #[inline]
    pub fn set_ep_kind(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 8;
        self
    }
    #[doc = "Bit 8 - Endpoint kind"]
    #[inline]
    pub fn ep_kind(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 8;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 9:10 - Endpoint type"]
    #[inline]
    pub unsafe fn ep_type(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 9;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 11 - Setup transaction completed"]
    #[inline]
    pub fn clear_setup(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 11);
        self
    }
    #[doc = "Bit 11 - Setup transaction completed"]
    #[inline]
    pub fn set_setup(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 11;
        self
    }
    #[doc = "Bit 11 - Setup transaction completed"]
    #[inline]
    pub fn setup(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 11;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bits 12:13 - Status bits, for reception transfers"]
    #[inline]
    pub unsafe fn stat_rx(&mut self, value: u8) -> &mut Self {
        const MASK: u8 = 3;
        const OFFSET: u8 = 12;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 14 - Data Toggle, for reception transfers"]
    #[inline]
    pub fn clear_dtog_rx(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 14);
        self
    }
    #[doc = "Bit 14 - Data Toggle, for reception transfers"]
    #[inline]
    pub fn set_dtog_rx(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 14;
        self
    }
    #[doc = "Bit 14 - Data Toggle, for reception transfers"]
    #[inline]
    pub fn dtog_rx(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 14;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 15 - Correct transfer for reception"]
    #[inline]
    pub fn clear_ctr_rx(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 15);
        self
    }
    #[doc = "Bit 15 - Correct transfer for reception"]
    #[inline]
    pub fn set_ctr_rx(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 15;
        self
    }
    #[doc = "Bit 15 - Correct transfer for reception"]
    #[inline]
    pub fn ctr_rx(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 15;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
