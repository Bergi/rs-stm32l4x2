#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
impl super::FNR {
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bits 0:10 - Frame number"]
    #[inline]
    pub fn fn_(&self) -> u16 {
        const MASK: u16 = 2047;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) as u16
    }
    #[doc = "Bits 11:12 - Lost SOF"]
    #[inline]
    pub fn lsof(&self) -> u8 {
        const MASK: u8 = 3;
        const OFFSET: u8 = 11;
        ((self.bits >> OFFSET) & MASK as u32) as u8
    }
    #[doc = "Bit 13 - Locked"]
    #[inline]
    pub fn lck(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 13;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 14 - Receive data - line status"]
    #[inline]
    pub fn rxdm(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 14;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 15 - Receive data + line status"]
    #[inline]
    pub fn rxdp(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 15;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
}
