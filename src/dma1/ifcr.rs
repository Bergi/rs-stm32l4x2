#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::IFCR {
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bit 27 - Channel x transfer error clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_cteif7(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 27);
        self
    }
    #[doc = "Bit 27 - Channel x transfer error clear (x = 1 ..7)"]
    #[inline]
    pub fn set_cteif7(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 27;
        self
    }
    #[doc = "Bit 27 - Channel x transfer error clear (x = 1 ..7)"]
    #[inline]
    pub fn cteif7(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 27;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 26 - Channel x half transfer clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_chtif7(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 26);
        self
    }
    #[doc = "Bit 26 - Channel x half transfer clear (x = 1 ..7)"]
    #[inline]
    pub fn set_chtif7(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 26;
        self
    }
    #[doc = "Bit 26 - Channel x half transfer clear (x = 1 ..7)"]
    #[inline]
    pub fn chtif7(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 26;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 25 - Channel x transfer complete clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_ctcif7(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 25);
        self
    }
    #[doc = "Bit 25 - Channel x transfer complete clear (x = 1 ..7)"]
    #[inline]
    pub fn set_ctcif7(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 25;
        self
    }
    #[doc = "Bit 25 - Channel x transfer complete clear (x = 1 ..7)"]
    #[inline]
    pub fn ctcif7(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 25;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 24 - Channel x global interrupt clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_cgif7(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 24);
        self
    }
    #[doc = "Bit 24 - Channel x global interrupt clear (x = 1 ..7)"]
    #[inline]
    pub fn set_cgif7(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 24;
        self
    }
    #[doc = "Bit 24 - Channel x global interrupt clear (x = 1 ..7)"]
    #[inline]
    pub fn cgif7(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 24;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 23 - Channel x transfer error clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_cteif6(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 23);
        self
    }
    #[doc = "Bit 23 - Channel x transfer error clear (x = 1 ..7)"]
    #[inline]
    pub fn set_cteif6(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 23;
        self
    }
    #[doc = "Bit 23 - Channel x transfer error clear (x = 1 ..7)"]
    #[inline]
    pub fn cteif6(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 23;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 22 - Channel x half transfer clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_chtif6(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 22);
        self
    }
    #[doc = "Bit 22 - Channel x half transfer clear (x = 1 ..7)"]
    #[inline]
    pub fn set_chtif6(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 22;
        self
    }
    #[doc = "Bit 22 - Channel x half transfer clear (x = 1 ..7)"]
    #[inline]
    pub fn chtif6(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 22;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 21 - Channel x transfer complete clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_ctcif6(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 21);
        self
    }
    #[doc = "Bit 21 - Channel x transfer complete clear (x = 1 ..7)"]
    #[inline]
    pub fn set_ctcif6(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 21;
        self
    }
    #[doc = "Bit 21 - Channel x transfer complete clear (x = 1 ..7)"]
    #[inline]
    pub fn ctcif6(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 21;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 20 - Channel x global interrupt clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_cgif6(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 20);
        self
    }
    #[doc = "Bit 20 - Channel x global interrupt clear (x = 1 ..7)"]
    #[inline]
    pub fn set_cgif6(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 20;
        self
    }
    #[doc = "Bit 20 - Channel x global interrupt clear (x = 1 ..7)"]
    #[inline]
    pub fn cgif6(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 20;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 19 - Channel x transfer error clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_cteif5(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 19);
        self
    }
    #[doc = "Bit 19 - Channel x transfer error clear (x = 1 ..7)"]
    #[inline]
    pub fn set_cteif5(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 19;
        self
    }
    #[doc = "Bit 19 - Channel x transfer error clear (x = 1 ..7)"]
    #[inline]
    pub fn cteif5(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 19;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 18 - Channel x half transfer clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_chtif5(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 18);
        self
    }
    #[doc = "Bit 18 - Channel x half transfer clear (x = 1 ..7)"]
    #[inline]
    pub fn set_chtif5(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 18;
        self
    }
    #[doc = "Bit 18 - Channel x half transfer clear (x = 1 ..7)"]
    #[inline]
    pub fn chtif5(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 18;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 17 - Channel x transfer complete clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_ctcif5(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 17);
        self
    }
    #[doc = "Bit 17 - Channel x transfer complete clear (x = 1 ..7)"]
    #[inline]
    pub fn set_ctcif5(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 17;
        self
    }
    #[doc = "Bit 17 - Channel x transfer complete clear (x = 1 ..7)"]
    #[inline]
    pub fn ctcif5(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 17;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 16 - Channel x global interrupt clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_cgif5(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 16);
        self
    }
    #[doc = "Bit 16 - Channel x global interrupt clear (x = 1 ..7)"]
    #[inline]
    pub fn set_cgif5(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 16;
        self
    }
    #[doc = "Bit 16 - Channel x global interrupt clear (x = 1 ..7)"]
    #[inline]
    pub fn cgif5(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 16;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 15 - Channel x transfer error clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_cteif4(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 15);
        self
    }
    #[doc = "Bit 15 - Channel x transfer error clear (x = 1 ..7)"]
    #[inline]
    pub fn set_cteif4(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 15;
        self
    }
    #[doc = "Bit 15 - Channel x transfer error clear (x = 1 ..7)"]
    #[inline]
    pub fn cteif4(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 15;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 14 - Channel x half transfer clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_chtif4(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 14);
        self
    }
    #[doc = "Bit 14 - Channel x half transfer clear (x = 1 ..7)"]
    #[inline]
    pub fn set_chtif4(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 14;
        self
    }
    #[doc = "Bit 14 - Channel x half transfer clear (x = 1 ..7)"]
    #[inline]
    pub fn chtif4(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 14;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 13 - Channel x transfer complete clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_ctcif4(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 13);
        self
    }
    #[doc = "Bit 13 - Channel x transfer complete clear (x = 1 ..7)"]
    #[inline]
    pub fn set_ctcif4(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 13;
        self
    }
    #[doc = "Bit 13 - Channel x transfer complete clear (x = 1 ..7)"]
    #[inline]
    pub fn ctcif4(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 13;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 12 - Channel x global interrupt clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_cgif4(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 12);
        self
    }
    #[doc = "Bit 12 - Channel x global interrupt clear (x = 1 ..7)"]
    #[inline]
    pub fn set_cgif4(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 12;
        self
    }
    #[doc = "Bit 12 - Channel x global interrupt clear (x = 1 ..7)"]
    #[inline]
    pub fn cgif4(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 12;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 11 - Channel x transfer error clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_cteif3(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 11);
        self
    }
    #[doc = "Bit 11 - Channel x transfer error clear (x = 1 ..7)"]
    #[inline]
    pub fn set_cteif3(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 11;
        self
    }
    #[doc = "Bit 11 - Channel x transfer error clear (x = 1 ..7)"]
    #[inline]
    pub fn cteif3(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 11;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 10 - Channel x half transfer clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_chtif3(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 10);
        self
    }
    #[doc = "Bit 10 - Channel x half transfer clear (x = 1 ..7)"]
    #[inline]
    pub fn set_chtif3(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 10;
        self
    }
    #[doc = "Bit 10 - Channel x half transfer clear (x = 1 ..7)"]
    #[inline]
    pub fn chtif3(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 10;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 9 - Channel x transfer complete clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_ctcif3(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 9);
        self
    }
    #[doc = "Bit 9 - Channel x transfer complete clear (x = 1 ..7)"]
    #[inline]
    pub fn set_ctcif3(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 9;
        self
    }
    #[doc = "Bit 9 - Channel x transfer complete clear (x = 1 ..7)"]
    #[inline]
    pub fn ctcif3(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 9;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 8 - Channel x global interrupt clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_cgif3(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 8);
        self
    }
    #[doc = "Bit 8 - Channel x global interrupt clear (x = 1 ..7)"]
    #[inline]
    pub fn set_cgif3(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 8;
        self
    }
    #[doc = "Bit 8 - Channel x global interrupt clear (x = 1 ..7)"]
    #[inline]
    pub fn cgif3(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 8;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 7 - Channel x transfer error clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_cteif2(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 7);
        self
    }
    #[doc = "Bit 7 - Channel x transfer error clear (x = 1 ..7)"]
    #[inline]
    pub fn set_cteif2(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 7;
        self
    }
    #[doc = "Bit 7 - Channel x transfer error clear (x = 1 ..7)"]
    #[inline]
    pub fn cteif2(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 7;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 6 - Channel x half transfer clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_chtif2(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 6);
        self
    }
    #[doc = "Bit 6 - Channel x half transfer clear (x = 1 ..7)"]
    #[inline]
    pub fn set_chtif2(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 6;
        self
    }
    #[doc = "Bit 6 - Channel x half transfer clear (x = 1 ..7)"]
    #[inline]
    pub fn chtif2(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 6;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 5 - Channel x transfer complete clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_ctcif2(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 5);
        self
    }
    #[doc = "Bit 5 - Channel x transfer complete clear (x = 1 ..7)"]
    #[inline]
    pub fn set_ctcif2(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 5;
        self
    }
    #[doc = "Bit 5 - Channel x transfer complete clear (x = 1 ..7)"]
    #[inline]
    pub fn ctcif2(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 5;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 4 - Channel x global interrupt clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_cgif2(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 4);
        self
    }
    #[doc = "Bit 4 - Channel x global interrupt clear (x = 1 ..7)"]
    #[inline]
    pub fn set_cgif2(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 4;
        self
    }
    #[doc = "Bit 4 - Channel x global interrupt clear (x = 1 ..7)"]
    #[inline]
    pub fn cgif2(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 4;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 3 - Channel x transfer error clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_cteif1(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 3);
        self
    }
    #[doc = "Bit 3 - Channel x transfer error clear (x = 1 ..7)"]
    #[inline]
    pub fn set_cteif1(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 3;
        self
    }
    #[doc = "Bit 3 - Channel x transfer error clear (x = 1 ..7)"]
    #[inline]
    pub fn cteif1(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 3;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 2 - Channel x half transfer clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_chtif1(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 2);
        self
    }
    #[doc = "Bit 2 - Channel x half transfer clear (x = 1 ..7)"]
    #[inline]
    pub fn set_chtif1(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 2;
        self
    }
    #[doc = "Bit 2 - Channel x half transfer clear (x = 1 ..7)"]
    #[inline]
    pub fn chtif1(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 1 - Channel x transfer complete clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_ctcif1(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 1);
        self
    }
    #[doc = "Bit 1 - Channel x transfer complete clear (x = 1 ..7)"]
    #[inline]
    pub fn set_ctcif1(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 1;
        self
    }
    #[doc = "Bit 1 - Channel x transfer complete clear (x = 1 ..7)"]
    #[inline]
    pub fn ctcif1(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 0 - Channel x global interrupt clear (x = 1 ..7)"]
    #[inline]
    pub fn clear_cgif1(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 0);
        self
    }
    #[doc = "Bit 0 - Channel x global interrupt clear (x = 1 ..7)"]
    #[inline]
    pub fn set_cgif1(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 0;
        self
    }
    #[doc = "Bit 0 - Channel x global interrupt clear (x = 1 ..7)"]
    #[inline]
    pub fn cgif1(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
