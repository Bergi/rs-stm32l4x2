#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::BSRR {
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bit 31 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn clear_br15(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 31);
        self
    }
    #[doc = "Bit 31 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn set_br15(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 31;
        self
    }
    #[doc = "Bit 31 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn br15(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 31;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 30 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn clear_br14(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 30);
        self
    }
    #[doc = "Bit 30 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn set_br14(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 30;
        self
    }
    #[doc = "Bit 30 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn br14(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 30;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 29 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn clear_br13(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 29);
        self
    }
    #[doc = "Bit 29 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn set_br13(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 29;
        self
    }
    #[doc = "Bit 29 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn br13(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 29;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 28 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn clear_br12(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 28);
        self
    }
    #[doc = "Bit 28 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn set_br12(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 28;
        self
    }
    #[doc = "Bit 28 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn br12(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 28;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 27 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn clear_br11(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 27);
        self
    }
    #[doc = "Bit 27 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn set_br11(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 27;
        self
    }
    #[doc = "Bit 27 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn br11(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 27;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 26 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn clear_br10(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 26);
        self
    }
    #[doc = "Bit 26 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn set_br10(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 26;
        self
    }
    #[doc = "Bit 26 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn br10(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 26;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 25 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn clear_br9(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 25);
        self
    }
    #[doc = "Bit 25 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn set_br9(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 25;
        self
    }
    #[doc = "Bit 25 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn br9(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 25;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 24 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn clear_br8(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 24);
        self
    }
    #[doc = "Bit 24 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn set_br8(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 24;
        self
    }
    #[doc = "Bit 24 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn br8(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 24;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 23 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn clear_br7(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 23);
        self
    }
    #[doc = "Bit 23 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn set_br7(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 23;
        self
    }
    #[doc = "Bit 23 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn br7(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 23;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 22 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn clear_br6(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 22);
        self
    }
    #[doc = "Bit 22 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn set_br6(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 22;
        self
    }
    #[doc = "Bit 22 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn br6(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 22;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 21 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn clear_br5(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 21);
        self
    }
    #[doc = "Bit 21 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn set_br5(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 21;
        self
    }
    #[doc = "Bit 21 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn br5(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 21;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 20 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn clear_br4(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 20);
        self
    }
    #[doc = "Bit 20 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn set_br4(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 20;
        self
    }
    #[doc = "Bit 20 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn br4(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 20;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 19 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn clear_br3(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 19);
        self
    }
    #[doc = "Bit 19 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn set_br3(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 19;
        self
    }
    #[doc = "Bit 19 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn br3(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 19;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 18 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn clear_br2(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 18);
        self
    }
    #[doc = "Bit 18 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn set_br2(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 18;
        self
    }
    #[doc = "Bit 18 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn br2(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 18;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 17 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn clear_br1(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 17);
        self
    }
    #[doc = "Bit 17 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn set_br1(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 17;
        self
    }
    #[doc = "Bit 17 - Port x reset bit y (y = 0..15)"]
    #[inline]
    pub fn br1(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 17;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 16 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn clear_br0(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 16);
        self
    }
    #[doc = "Bit 16 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn set_br0(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 16;
        self
    }
    #[doc = "Bit 16 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn br0(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 16;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 15 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn clear_bs15(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 15);
        self
    }
    #[doc = "Bit 15 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn set_bs15(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 15;
        self
    }
    #[doc = "Bit 15 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn bs15(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 15;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 14 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn clear_bs14(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 14);
        self
    }
    #[doc = "Bit 14 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn set_bs14(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 14;
        self
    }
    #[doc = "Bit 14 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn bs14(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 14;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 13 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn clear_bs13(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 13);
        self
    }
    #[doc = "Bit 13 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn set_bs13(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 13;
        self
    }
    #[doc = "Bit 13 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn bs13(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 13;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 12 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn clear_bs12(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 12);
        self
    }
    #[doc = "Bit 12 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn set_bs12(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 12;
        self
    }
    #[doc = "Bit 12 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn bs12(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 12;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 11 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn clear_bs11(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 11);
        self
    }
    #[doc = "Bit 11 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn set_bs11(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 11;
        self
    }
    #[doc = "Bit 11 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn bs11(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 11;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 10 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn clear_bs10(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 10);
        self
    }
    #[doc = "Bit 10 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn set_bs10(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 10;
        self
    }
    #[doc = "Bit 10 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn bs10(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 10;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 9 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn clear_bs9(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 9);
        self
    }
    #[doc = "Bit 9 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn set_bs9(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 9;
        self
    }
    #[doc = "Bit 9 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn bs9(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 9;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 8 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn clear_bs8(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 8);
        self
    }
    #[doc = "Bit 8 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn set_bs8(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 8;
        self
    }
    #[doc = "Bit 8 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn bs8(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 8;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 7 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn clear_bs7(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 7);
        self
    }
    #[doc = "Bit 7 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn set_bs7(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 7;
        self
    }
    #[doc = "Bit 7 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn bs7(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 7;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 6 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn clear_bs6(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 6);
        self
    }
    #[doc = "Bit 6 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn set_bs6(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 6;
        self
    }
    #[doc = "Bit 6 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn bs6(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 6;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 5 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn clear_bs5(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 5);
        self
    }
    #[doc = "Bit 5 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn set_bs5(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 5;
        self
    }
    #[doc = "Bit 5 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn bs5(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 5;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 4 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn clear_bs4(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 4);
        self
    }
    #[doc = "Bit 4 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn set_bs4(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 4;
        self
    }
    #[doc = "Bit 4 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn bs4(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 4;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 3 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn clear_bs3(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 3);
        self
    }
    #[doc = "Bit 3 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn set_bs3(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 3;
        self
    }
    #[doc = "Bit 3 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn bs3(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 3;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 2 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn clear_bs2(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 2);
        self
    }
    #[doc = "Bit 2 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn set_bs2(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 2;
        self
    }
    #[doc = "Bit 2 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn bs2(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 1 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn clear_bs1(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 1);
        self
    }
    #[doc = "Bit 1 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn set_bs1(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 1;
        self
    }
    #[doc = "Bit 1 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn bs1(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 0 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn clear_bs0(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 0);
        self
    }
    #[doc = "Bit 0 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn set_bs0(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 0;
        self
    }
    #[doc = "Bit 0 - Port x set bit y (y= 0..15)"]
    #[inline]
    pub fn bs0(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
