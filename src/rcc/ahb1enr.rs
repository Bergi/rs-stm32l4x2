#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::AHB1ENR {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bit 16 - Touch Sensing Controller clock enable"]
    #[inline]
    pub fn tscen(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 16;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 12 - CRC clock enable"]
    #[inline]
    pub fn crcen(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 12;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 8 - Flash memory interface clock enable"]
    #[inline]
    pub fn flashen(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 8;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 1 - DMA2 clock enable"]
    #[inline]
    pub fn dma2en(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 0 - DMA1 clock enable"]
    #[inline]
    pub fn dma1en(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 256 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bit 16 - Touch Sensing Controller clock enable"]
    #[inline]
    pub fn clear_tscen(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 16);
        self
    }
    #[doc = "Bit 16 - Touch Sensing Controller clock enable"]
    #[inline]
    pub fn set_tscen(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 16;
        self
    }
    #[doc = "Bit 16 - Touch Sensing Controller clock enable"]
    #[inline]
    pub fn tscen(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 16;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 12 - CRC clock enable"]
    #[inline]
    pub fn clear_crcen(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 12);
        self
    }
    #[doc = "Bit 12 - CRC clock enable"]
    #[inline]
    pub fn set_crcen(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 12;
        self
    }
    #[doc = "Bit 12 - CRC clock enable"]
    #[inline]
    pub fn crcen(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 12;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 8 - Flash memory interface clock enable"]
    #[inline]
    pub fn clear_flashen(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 8);
        self
    }
    #[doc = "Bit 8 - Flash memory interface clock enable"]
    #[inline]
    pub fn set_flashen(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 8;
        self
    }
    #[doc = "Bit 8 - Flash memory interface clock enable"]
    #[inline]
    pub fn flashen(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 8;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 1 - DMA2 clock enable"]
    #[inline]
    pub fn clear_dma2en(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 1);
        self
    }
    #[doc = "Bit 1 - DMA2 clock enable"]
    #[inline]
    pub fn set_dma2en(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 1;
        self
    }
    #[doc = "Bit 1 - DMA2 clock enable"]
    #[inline]
    pub fn dma2en(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 0 - DMA1 clock enable"]
    #[inline]
    pub fn clear_dma1en(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 0);
        self
    }
    #[doc = "Bit 0 - DMA1 clock enable"]
    #[inline]
    pub fn set_dma1en(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 0;
        self
    }
    #[doc = "Bit 0 - DMA1 clock enable"]
    #[inline]
    pub fn dma1en(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
