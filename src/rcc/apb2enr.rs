#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::APB2ENR {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bit 21 - SAI1 clock enable"]
    #[inline]
    pub fn sai1en(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 21;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 17 - TIM16 timer clock enable"]
    #[inline]
    pub fn tim16en(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 17;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 16 - TIM15 timer clock enable"]
    #[inline]
    pub fn tim15en(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 16;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 14 - USART1 clock enable"]
    #[inline]
    pub fn usart1en(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 14;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 12 - SPI1 clock enable"]
    #[inline]
    pub fn spi1en(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 12;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 11 - TIM1 timer clock enable"]
    #[inline]
    pub fn tim1en(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 11;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 10 - SDMMC clock enable"]
    #[inline]
    pub fn sdmmcen(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 10;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 7 - Firewall clock enable"]
    #[inline]
    pub fn firewallen(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 7;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 0 - SYSCFG clock enable"]
    #[inline]
    pub fn syscfgen(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bit 21 - SAI1 clock enable"]
    #[inline]
    pub fn clear_sai1en(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 21);
        self
    }
    #[doc = "Bit 21 - SAI1 clock enable"]
    #[inline]
    pub fn set_sai1en(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 21;
        self
    }
    #[doc = "Bit 21 - SAI1 clock enable"]
    #[inline]
    pub fn sai1en(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 21;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 17 - TIM16 timer clock enable"]
    #[inline]
    pub fn clear_tim16en(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 17);
        self
    }
    #[doc = "Bit 17 - TIM16 timer clock enable"]
    #[inline]
    pub fn set_tim16en(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 17;
        self
    }
    #[doc = "Bit 17 - TIM16 timer clock enable"]
    #[inline]
    pub fn tim16en(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 17;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 16 - TIM15 timer clock enable"]
    #[inline]
    pub fn clear_tim15en(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 16);
        self
    }
    #[doc = "Bit 16 - TIM15 timer clock enable"]
    #[inline]
    pub fn set_tim15en(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 16;
        self
    }
    #[doc = "Bit 16 - TIM15 timer clock enable"]
    #[inline]
    pub fn tim15en(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 16;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 14 - USART1 clock enable"]
    #[inline]
    pub fn clear_usart1en(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 14);
        self
    }
    #[doc = "Bit 14 - USART1 clock enable"]
    #[inline]
    pub fn set_usart1en(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 14;
        self
    }
    #[doc = "Bit 14 - USART1 clock enable"]
    #[inline]
    pub fn usart1en(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 14;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 12 - SPI1 clock enable"]
    #[inline]
    pub fn clear_spi1en(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 12);
        self
    }
    #[doc = "Bit 12 - SPI1 clock enable"]
    #[inline]
    pub fn set_spi1en(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 12;
        self
    }
    #[doc = "Bit 12 - SPI1 clock enable"]
    #[inline]
    pub fn spi1en(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 12;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 11 - TIM1 timer clock enable"]
    #[inline]
    pub fn clear_tim1en(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 11);
        self
    }
    #[doc = "Bit 11 - TIM1 timer clock enable"]
    #[inline]
    pub fn set_tim1en(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 11;
        self
    }
    #[doc = "Bit 11 - TIM1 timer clock enable"]
    #[inline]
    pub fn tim1en(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 11;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 10 - SDMMC clock enable"]
    #[inline]
    pub fn clear_sdmmcen(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 10);
        self
    }
    #[doc = "Bit 10 - SDMMC clock enable"]
    #[inline]
    pub fn set_sdmmcen(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 10;
        self
    }
    #[doc = "Bit 10 - SDMMC clock enable"]
    #[inline]
    pub fn sdmmcen(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 10;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 7 - Firewall clock enable"]
    #[inline]
    pub fn clear_firewallen(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 7);
        self
    }
    #[doc = "Bit 7 - Firewall clock enable"]
    #[inline]
    pub fn set_firewallen(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 7;
        self
    }
    #[doc = "Bit 7 - Firewall clock enable"]
    #[inline]
    pub fn firewallen(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 7;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 0 - SYSCFG clock enable"]
    #[inline]
    pub fn clear_syscfgen(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 0);
        self
    }
    #[doc = "Bit 0 - SYSCFG clock enable"]
    #[inline]
    pub fn set_syscfgen(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 0;
        self
    }
    #[doc = "Bit 0 - SYSCFG clock enable"]
    #[inline]
    pub fn syscfgen(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
