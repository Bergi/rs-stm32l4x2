#[doc = r" Value read from the register"]
pub struct R {
    bits: u32,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u32,
}
impl super::CCR5 {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u32 {
        self.bits
    }
    #[doc = "Bits 0:15 - Capture/Compare value"]
    #[inline]
    pub fn ccr5(&self) -> u16 {
        const MASK: u16 = 65535;
        const OFFSET: u8 = 0;
        ((self.bits >> OFFSET) & MASK as u32) as u16
    }
    #[doc = "Bit 29 - Group Channel 5 and Channel 1"]
    #[inline]
    pub fn gc5c1(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 29;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 30 - Group Channel 5 and Channel 2"]
    #[inline]
    pub fn gc5c2(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 30;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
    #[doc = "Bit 31 - Group Channel 5 and Channel 3"]
    #[inline]
    pub fn gc5c3(&self) -> bool {
        const MASK: bool = true;
        const OFFSET: u8 = 31;
        ((self.bits >> OFFSET) & MASK as u32) != 0
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u32) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bits 0:15 - Capture/Compare value"]
    #[inline]
    pub unsafe fn ccr5(&mut self, value: u16) -> &mut Self {
        const MASK: u16 = 65535;
        const OFFSET: u8 = 0;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 29 - Group Channel 5 and Channel 1"]
    #[inline]
    pub fn clear_gc5c1(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 29);
        self
    }
    #[doc = "Bit 29 - Group Channel 5 and Channel 1"]
    #[inline]
    pub fn set_gc5c1(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 29;
        self
    }
    #[doc = "Bit 29 - Group Channel 5 and Channel 1"]
    #[inline]
    pub fn gc5c1(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 29;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 30 - Group Channel 5 and Channel 2"]
    #[inline]
    pub fn clear_gc5c2(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 30);
        self
    }
    #[doc = "Bit 30 - Group Channel 5 and Channel 2"]
    #[inline]
    pub fn set_gc5c2(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 30;
        self
    }
    #[doc = "Bit 30 - Group Channel 5 and Channel 2"]
    #[inline]
    pub fn gc5c2(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 30;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
    #[doc = "Bit 31 - Group Channel 5 and Channel 3"]
    #[inline]
    pub fn clear_gc5c3(&mut self) -> &mut Self {
        self.bits &= !((true as u32) << 31);
        self
    }
    #[doc = "Bit 31 - Group Channel 5 and Channel 3"]
    #[inline]
    pub fn set_gc5c3(&mut self) -> &mut Self {
        self.bits |= (true as u32) << 31;
        self
    }
    #[doc = "Bit 31 - Group Channel 5 and Channel 3"]
    #[inline]
    pub fn gc5c3(&mut self, value: bool) -> &mut Self {
        const MASK: bool = true;
        const OFFSET: u8 = 31;
        self.bits &= !((MASK as u32) << OFFSET);
        self.bits |= ((value & MASK) as u32) << OFFSET;
        self
    }
}
